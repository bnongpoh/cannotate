# Mutation Criteria
- DC  (Decision Coverage)
- MCC (Multiple Condition Coverage)
- ABS (Absolute Value Insertion)
- AOR (Arithmetic Operator Replacement)
- COR (Conditional Operator Replacement)
- ROR (Relational Operator Replacement)

## Decision Coverage (DC)
Requires a test suite to activate 
**both the true and false path** of each decision point
in the program under test.

## Multiple Condition Coverage (MCC)
Requires a test suite to activate **all the combinations of truth values**
of **all atomic conditions** at each decision point in the program

```c
if (a && b) { /***/}

∆1 ( a &&  b)
∆2 (!a &&  b)
∆3 ( a && !b)
∆4 (!a && !b)
```

## Absolute Value Insertion (ABS)
Each arithmetic expression (and subexpression) is modified by the functions
`abs()`, `negAbs()`, and `failOnZero()`

```c
   a = m * (o + p);
∆1 a = abs (m * (o + p));
∆2 a = m * abs ((o + p));
∆3 a = failOnZero (m * (o + p));
```

## Arithmetic Operator Replacement (AOR)
Each occurrence of one of the arithmetic operators +, －,\*, ／, and % is
replaced by each of the other operators. In addition, each is replaced by the
special mutation operators leftOp, and rightOp.

```c
   a = m * (o + p);
∆1 a = m + (o + p);
∆2 a = m * (o * p);
∆3 a = m leftOp (o + p);
```

## Conditional Operator Replacement (COR)
Each occurrence of one of the logical operators (and - &&, or - || , and with no
conditional evaluation - &, or with no conditional evaluation - |, not equivalent - ^) 
is replaced by each of the other operators; in addition, each is replaced by
falseOp, trueOp, leftOp, and rightOp.
```c
   if (X <= Y && a > 0)
∆1 if (X <= Y || a > 0)
∆2 if (X <= Y leftOp a > 0) // returns result of left clause
```

## Relational Operator Replacement (ROR)
Each occurrence of one of the relational operators (<, ≤, >, ≥, =, ≠) is replaced
by each of the other operators and by falseOp and trueOp.
```c
   if (X <= Y)
∆1 if (X > Y)
∆2 if (X < Y)
∆3 if (X falseOp Y) // always returns false
```

