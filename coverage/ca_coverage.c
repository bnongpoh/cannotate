#ifndef CA_COV_H
#define CA_COV_H 1

#ifndef __GNUC__
#define __asm__ asm
#endif

#ifdef __clang__
void __attribute__ ((optnone)) report_label_coverage(char * crit, int id, char * name)
{
	__asm__ volatile (""::: "memory");
}
#else
#ifdef __GNUC__
void __attribute__ ((optimize("O0"))) report_label_coverage(char * crit, int id, char * name)
{
	__asm__ volatile (""::: "memory");
}
#else
void report_label_coverage(char * crit, int id, char * name)
{
	__asm__ volatile (""::: "memory");
}
#endif
#endif

#endif
