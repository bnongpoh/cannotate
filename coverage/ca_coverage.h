
#ifndef __GNUC__
#define __asm__ asm
#endif

#ifdef __clang__
void __attribute__ ((optnone)) report_label_coverage(char * crit, int id, char * name) __asm__("report_label_coverage");
#else
#ifdef __GNUC__
void __attribute__ ((optimize("O0"))) report_label_coverage(char * crit, int id, char * name) __asm__("report_label_coverage");
#else
void report_label_coverage(char * crit, int id, char * name) __asm__("report_label_coverage");
#endif
#endif
