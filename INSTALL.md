# Installation

### **Requirements**

- LLVM + Clang 13  and above
- CMake  3.0 and above

### **System Tested**
- Ubuntu 20 LTS

### **Installing LLVM**
### Installing from Source
You may follow the official link for installation from the source [Getting the source and building LLVM](https://llvm.org/docs/GettingStarted.html#getting-the-source-code-and-building-llvm)
### Pre-compiled Binaries
Download the pre-compiled binaries from the official GitHub repository of LLVM
```bash
wget https://github.com/llvm/llvm-project/releases/download/llvmorg-13.0.0/clang+llvm-13.0.0-x86_64-linux-gnu-ubuntu-20.04.tar.xz
tar -xvf clang+llvm-13.0.0-x86_64-linux-gnu-ubuntu-20.04.tar.xz
```
**Environment variables and PATH**
`sudo vi ~/.bashrc`
```bash
export LLVM_CONFIG=/path/to/llvm/clang+llvm-13.0.0-x86_64-linux-gnu-ubuntu-20.04/bin/llvm-config
export LLVM_DIR=/path/to/llvm/clang+llvm-13.0.0-x86_64-linux-gnu-ubuntu-20.04
export PATH="$PATH:$LLVM_DIR/bin"
```
`source ~/.bashrc`
### 
```bash
clang -v
clang version 13.0.0 (https://github.com/llvm/llvm-project/ 24c8eaec9467b2aaf70b0db33a4e4dd415139a50)
Target: x86_64-unknown-linux-gnu
Thread model: posix
InstalledDir: /home/bernard/LLVM/clang+llvm-13.0.0-x86_64-linux-gnu-ubuntu-20.04/bin
Found candidate GCC installation: /usr/lib/gcc/x86_64-linux-gnu/9
Selected GCC installation: /usr/lib/gcc/x86_64-linux-gnu/9
Candidate multilib: .;@m64
Candidate multilib: 32;@m32
Candidate multilib: x32;@mx32
Selected multilib: .;@m64
```
If the output is similar to the above, you’re successfully installed LLVM and Clang.

### Installing Cmake

```bash
sudo apt-get -y install cmake
cmake --version
cmake version 3.23.0
sudo ln -s /usr/lib/x86_64-linux-gnu/libtinfo.so.6 /usr/lib/x86_64-linux-gnu/libtinfo.so
``` 

### Download the CAnnotate Tool from GitLab

```bash
git clone
```

```bash
cd CAnnotate
CAnnotate$ mkdir build
CAnnotate$ cd build
$CAnnotate/build$ cmake ..
Consolidate compiler generated dependencies of target CAnnotate
[100%] Built target CAnnotate
```
### Environment Setup 
Export the PATH to the binaries to make it available globally. 
```
vi ~/.bashrc
 export CANNOTATE=/path/to/Cannotate/build/src/
 export PATH=$PATH:$CANNOTATE/side-effect
 export PATH=$PATH:$CANNOTATE/cannotate
 export PATH=$PATH:$CANNOTATE/../../compiler-wrappers
```
