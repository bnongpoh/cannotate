#!/bin/bash -e

docker build  -t "cannotate-demo" \
    --build-arg USER_ID=$(id -u $USER) \
    --build-arg GROUP_ID=$(id -g $USER) \
	-f "docker/Dockerfile" .

