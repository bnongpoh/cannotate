int foo()
{
    return 0;
}

int main(){

int temp = foo() ;


// ABS Label 1
if(temp < 0)  {
asm volatile (""::: "memory");
	report_label_coverage("ABS", 1);
}

// COR Label 2
if((temp > 1 || temp < 10) != (temp > 1 && temp < 10))  {
asm volatile (""::: "memory");
	report_label_coverage("COR", 2);
}

// ROR Label 3
if((temp <= 1) != (temp > 1))  {
asm volatile (""::: "memory");
	report_label_coverage("ROR", 3);
}
// ROR Label 4
if((temp < 1) != (temp > 1))  {
asm volatile (""::: "memory");
	report_label_coverage("ROR", 4);
}
// ROR Label 5
if((temp >= 1) != (temp > 1))  {
asm volatile (""::: "memory");
	report_label_coverage("ROR", 5);
}
// ROR Label 6
if((temp <= 10) != (temp < 10))  {
asm volatile (""::: "memory");
	report_label_coverage("ROR", 6);
}
// ROR Label 7
if((temp > 10) != (temp < 10))  {
asm volatile (""::: "memory");
	report_label_coverage("ROR", 7);
}
// ROR Label 8
if((temp >= 10) != (temp < 10))  {
asm volatile (""::: "memory");
	report_label_coverage("ROR", 8);
}

// MCC Label 9
if(temp > 1 && temp < 10 ) {
asm volatile (""::: "memory");
	report_label_coverage("MCC", 9);
}
// MCC Label 10
if(temp > 1 && !(temp < 10) ) {
asm volatile (""::: "memory");
	report_label_coverage("MCC", 10);
}
// MCC Label 11
if(!(temp > 1) && temp < 10 ) {
asm volatile (""::: "memory");
	report_label_coverage("MCC", 11);
}
// MCC Label 12
if(!(temp > 1) && !(temp < 10) ) {
asm volatile (""::: "memory");
	report_label_coverage("MCC", 12);
}
if (temp>1 && temp<10) { }
return 0;
}

