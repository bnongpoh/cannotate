#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int pass(int input) {
    return input;
}

void noelse(int a, int b, int c) {
    if (pass(a) == pass(b) && pass(c)) {
        return;
    }
}

void combine1(int a, int b, int c) {
    printf("a=%d, b=%d, c=%d, ", a, b, c);

    if ((pass(a) && pass(b)) || pass(c)) {
        printf("(a && b) || c\n");
        return;
    } else {
        printf("!((a && b) || c)\n");
        return;
    }
}

void combine2(int a, int b, int c) {
    printf("a=%d, b=%d, c=%d, ", a, b, c);

    if ((pass(a) && pass(b)) || pass(c)) {
        printf("(a && b) || c\n");
        return;
    } else {
        printf("!((a && b) || c)\n");
        return;
    }
}

void combine3(int a, int b, int c) {
    printf("a=%d, b=%d, c=%d, ", a, b, c);

    if (pass(a) && (pass(b) || pass(c))) {
        printf("a && (b || c)\n");
        return;
    } else {
        printf("!(a && (b || c))\n");
        return;
    }
}

void combine4(int a, int b, int c) {
    printf("a=%d, b=%d, c=%d, ", a, b, c);

    if (pass(a) && !(pass(b) || pass(c))) {
        printf("a && !(b || c)\n");
        return;
    } else {
        printf("!(a && !(b || c))\n");
        return;
    }
}

void combine5(int a, int b, int c) {
    printf("a=%d, b=%d, c=%d, ", a, b, c);

    if (!(pass(a) && !pass(b)) || pass(c)) {
        printf("!(a && !b) || c\n");
        return;
    } else {
        printf("!(!(a && !b) || c)\n");
        return;
    }
}

void combine6(int a, int b, int c, int d) {
    printf("a=%d, b=%d, c=%d, d=%d, ", a, b, c, d);

    if (!(pass(a) && !pass(b)) || (pass(c) && pass(d))) {
        printf("!(a || !b) || (c && d)\n");
        return;
    } else {
        printf("!(!(a || !b) || (c && d))\n");
        return;
    }
}

void and(int a, int b) {
    printf("a=%d, b=%d ", a, b);

    if (pass(a) && pass(b)) {
        printf("a && b\n");
        return;
    } else {
        printf("!(a && b)\n");
        return;
    }
}

void and2(int a, int b, int c) {
    printf("a=%d, b=%d, c=%d, ", a, b, c);

    if (pass(a) && pass(b) && pass(c)) {
        printf("a && b && c\n");
        return;
    } else {
        printf("!(a && b && c)\n");
        return;
    }
}

void or(int a, int b) {
    printf("a=%d, b=%d, ", a, b);
    if (pass(a) || pass(b)) {
        printf("a || b\n");
        return;
    } else {
        printf("!(a || b)\n");
        return;
    }
}

void or2(int a, int b, int c) {
    printf("a=%d, b=%d, c=%d, ", a, b, c);

    if (pass(a) || pass(b) || pass(c)) {
        printf("a || b || c\n");
        return;
    } else {
        printf("!(a || b || c)\n");
        return;
    }
}

void case1(int a, int b, int c) {
    printf("a=%d, b=%d, c=%d, ", a, b, c);

    if (pass(a) || pass(b) || pass(c)) {
        printf("a || b || c\n");
        if (c > 0) {
            printf("\ta || b || c and c > 0\n");
            return;
        }
    } else {
        printf("!(a || b || c)\n");
    }
}

int main() {

    and(1, 1);
    and(1, 0);

    and2(1, 1, 1);
    and2(1, 0, 1);
    and2(1, 1, 0);
    and2(0, 1, 1);
    and2(1, 0, 0);
    and2(0, 1, 0);
    and2(0, 0, 1);
    and2(0, 0, 0);

    or(1, 1);
    or(1, 0);

    or2(1, 1, 1);
    or2(1, 0, 1);
    or2(0, 1, 1);
    or2(1, 1, 0);
    or2(1, 0, 0);
    or2(0, 1, 0);
    or2(0, 0, 1);
    or2(0, 0, 0);

    case1(1, 1, 1);
    case1(1, 0, 1);
    case1(0, 1, 1);
    case1(1, 1, 0);
    case1(1, 0, 0);
    case1(0, 1, 0);
    case1(0, 0, 1);
    case1(0, 0, 0);

    combine1(1, 1, 1);
    combine1(1, 0, 1);
    combine1(0, 1, 1);
    combine1(1, 1, 0);
    combine1(1, 0, 0);
    combine1(0, 1, 0);
    combine1(0, 0, 1);
    combine1(0, 0, 0);

    combine2(1, 1, 1);
    combine2(1, 0, 1);
    combine2(0, 1, 1);
    combine2(1, 1, 0);
    combine2(1, 0, 0);
    combine2(0, 1, 0);
    combine2(0, 0, 1);
    combine2(0, 0, 0);

    combine3(1, 1, 1);
    combine3(1, 0, 1);
    combine3(0, 1, 1);
    combine3(1, 1, 0);
    combine3(1, 0, 0);
    combine3(0, 1, 0);
    combine3(0, 0, 1);
    combine3(0, 0, 0);

    combine4(1, 1, 1);
    combine4(1, 0, 1);
    combine4(0, 1, 1);
    combine4(1, 1, 0);
    combine4(1, 0, 0);
    combine4(0, 1, 0);
    combine4(0, 0, 1);
    combine4(0, 0, 0);

    combine5(1, 1, 1);
    combine5(1, 0, 1);
    combine5(0, 1, 1);
    combine5(1, 1, 0);
    combine5(1, 0, 0);
    combine5(0, 1, 0);
    combine5(0, 0, 1);
    combine5(0, 0, 0);

    combine6(1, 1, 1, 1);
    combine6(1, 0, 1, 1);
    combine6(0, 1, 1, 1);
    combine6(1, 1, 0, 1);
    combine6(1, 0, 0, 1);
    combine6(0, 1, 0, 1);
    combine6(0, 0, 1, 1);
    combine6(0, 0, 0, 1);
    combine6(1, 1, 1, 0);
    combine6(1, 0, 1, 0);
    combine6(0, 1, 1, 0);
    combine6(1, 1, 0, 0);
    combine6(1, 0, 0, 0);
    combine6(0, 1, 0, 0);
    combine6(0, 0, 1, 0);
    combine6(0, 0, 0, 0);

    return 0;
}

