#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define NANGLES 3
#define INTEGER int

int func(int a, int b, int c) {
    int i;
    for (i = a; i > 0;i -- ) printf("1\n");

    for (i = NANGLES; i > 0; i --) {
        printf("2\n");
    }

    for (; i < sqrt(a+b); i++) printf("3\n");

    for (int j = a + i; j > 0;j -- ) printf("4\n");

    for (int j = NANGLES + b; j > 0;j -- ) printf("5\n");

    for (INTEGER j = NANGLES + a; j > 0; j -- ) printf("6\n");
}


int main() {
    func(2, 1, 1);
    return 0;
}
