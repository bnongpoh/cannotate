#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <sys/types.h>

typedef struct st ST;
typedef struct sst S;
struct sst {
    int x;
    int y;
}
struct st {
    int x;
    float y;
    ST* ptr;
    S arr;
};

int outside(int x, int y);

int func() {
    ST *s1=NULL, *s2=NULL;
    s1 = malloc(sizeof(ST));
    s1->x = 0;
    s1->ptr = malloc(sizeof(ST));
    s1->ptr->x = 0;
    s1->ptr->y = 0;
    s1->arr.x = 1;
    s1->arr.y = 1;
    if (s1->ptr->x && s2->arr.x) return 0;
    if ((!s1->ptr->x && (int)(s1->arr.x)) || ((int)s2->arr.y & (1<<6))) return 0;
    else return 1;
}

int main() {
    func();
    return 0;
}

