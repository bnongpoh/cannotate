#!/bin/bash
set -e

if [ -f /usr/bin/side-effect ] && [ -f /usr/bin/cannotate ] && [ -f /usr/bin/cannotate-cc ]; then
	exit
fi

cd "$(dirname "${BASH_SOURCE[0]}")/" >/dev/null 2>&1


#apt-get update --fix-missing -y && \
#	apt-get install -y apt-transport-https ca-certificates
#apt-get install -y lsb-release software-properties-common \
#	make build-essential git wget\
#	zlib1g-dev libtinfo-dev ninja-build libssl-dev\
#	cmake
apt-get install -y zlib1g-dev libtinfo-dev wget clang-tidy

# Download clang-13 for compiling CAnnotate
wget https://github.com/llvm/llvm-project/releases/download/llvmorg-13.0.0/clang+llvm-13.0.0-x86_64-linux-gnu-ubuntu-16.04.tar.xz
tar -xvf clang+llvm-13.0.0-x86_64-linux-gnu-ubuntu-16.04.tar.xz > /dev/null
mv clang+llvm-13.0.0-x86_64-linux-gnu-ubuntu-16.04/ clang+llvm-13
export LLVM_DIR="$(pwd)/clang+llvm-13"
export LLVM_CONFIG=${LLVM_DIR}/bin/llvm-config
export PATH="$LLVM_DIR/bin:$PATH"

# Then build Cannotate
export CC=$LLVM_DIR/bin/clang
export CXX=$LLVM_DIR/bin/clang++
export CFLAGS="-I $LLVM_DIR/lib/"
export CXXFLAGS="-I $LLVM_DIR/lib/"
export LD_LIBRARY_PATH=$LLVM_DIR/lib/
export CMAKE_PREFIX_PATH=$LLVM_DIR/lib/cmake/clang/

rm -rf build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Debug ..
make -j${nproc}

CANNOTATE=$(pwd)/src/
rm -rf /usr/bin/side-effect /usr/bin/cannotate /usr/bin/cannotate-cc
ln -s $CANNOTATE/side-effect/side-effect                  /usr/bin/side-effect 
ln -s $CANNOTATE/cannotate/cannotate                      /usr/bin/cannotate   
ln -s $CANNOTATE/../../compiler-wrappers/cannotate-cc.sh  /usr/bin/cannotate-cc
