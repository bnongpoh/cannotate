#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define NANGLES 3
#define INTEGER int


typedef struct st ST;
struct st {
    int valid;
};

int inner(int a, int b) {
    return a + b;
}

int func(int a, int b, ST* c) {
    int i;
    if (a > 0 && b == 1) {
        return 1;
    } else if (inner(a, b+1) > 0) {
        return -2;
    }
    for (i = 0; i < 5; i++) {
        if (c == NULL || !c->valid) {
            continue;
        }
    }
    return 0;
}


int main() {
    if (func(2, 1, NULL) > 0) {
        printf("hello\n");
    }
    return 0;
}

