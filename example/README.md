# CAnnotate example

This is an example of using CAnnotate.

After installing CAnnotate (or use our provided Dockerfile), simply
run `make` in this directory. You will see the original `example.c`
being instrumented with labels. The original file is compiled to
`example_orig.out` and labeled file compiled to `example_labeled.out`.

You can also notice that the original c file will be back-up as
`example.tmp.orig`, the normalized file (file processed by `side-effect`)
is saved as `example.tmp.sf`.

Feel free to tweak the environment variables in `ca` section of 
`Makefile` to see the differences.
