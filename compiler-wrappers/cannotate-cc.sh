#!/bin/bash
##
#  CAnnotate as a wrapper for compilers
# 
#  @author  Wei-Cheng Wu (wwu_at_isi.edu)
#  @date    Created:        2022-05-25
#           Last modified:  2022-06-30
# 
# Environment variables:
# - env CRIT: state which coverage criteria to use. Default: all
# - env ORIG_CC: the original compiler to involve after annotation. Default: gcc
# - env WHITELIST: the files to perform cannotate, overwrites SKIPLIST
# - env SKIPLIST: the files directly sent to ORIG_CC without annotation
# - env SKIPFUNCS: the function names that will be skipped when annotating
# - env ADDITIONAL_FLAGS: use to pass any additional thing as compiling flag
##
set -e

if [ -z $CRIT ]; then
	export CRIT="ABS,AOR,COR,ROR,MCC"
fi

if [ -z $ORIG_CC ]; then
	export ORIG_CC=gcc
fi

# A list of file not annotating, directly pass to orig-compiler
SKIPLIST="$SKIPLIST conftest.c CMakeFiles/"

FLAGS="-I/usr/include/ -I/usr/local/include/ $ADDITIONAL_FLAGS"
CFILES=
NO_COMPILE=0

if [[ -z $DEBUG ]]; then
	DEBUG=0
fi

if [[ -z $COV || $COV -eq 0 ]]; then
	DO_COV=""
else
	DO_COV="-f cov"
fi

RED='\033[1;31m'
GREEN='\033[0;32m'
BGREEN='\033[1;32m'
BLUE='\033[0;34m'
NC='\033[0m'

file_no_annotate() {
	file=$1
	if $(grep -Fq "/*** Processed by Side-Effect Tool ***/" $file); then
		return 1
	fi
	if $(grep -Fq "/*** Processed by CAnnotate ***/" $file); then
		return 1
	fi
	return 0
}

# Function:    cannotate_file()
#    Main routine of annotating a file
#
# GLOBALS:   
#    INCLUDES, FLAGS, GREEN, BLUE, NC
# ARGUMENTS: 
#    $1 file
#
cannotate_file() {
	file=$1
	file_no_annotate $file && cp $file ${file}.tmp.orig

	# Perform side-effect if haven't run before
	if ! $(grep -Fq "/*** Processed by Side-Effect Tool ***/" $file); then
		echo -e "${GREEN}-> Running ${BGREEN}Side-effect${NC} on $file..."
		# Perform clang-tidy first
		if [ $DEBUG -eq 1 ]; then 
			set -x
			clang-tidy -fix\
				-checks="readability-braces-around-statements,\
						 readability-isolate-declaration" \
				-fix-errors\
				$file -- $FLAGS > /dev/null
		else
			clang-tidy  -fix\
				-checks="readability-braces-around-statements,\
						 readability-isolate-declaration" \
				-fix-errors\
				$file -- $FLAGS > /dev/null 2>&1
		fi
		# Then side-effect
		if [ $DEBUG -eq 1 ]; then 
			side-effect $file -o ${file}.tmp.sf -D -- $FLAGS
		else
			side-effect $file -o ${file}.tmp.sf -- $FLAGS > /dev/null
		fi
		cp ${file}.tmp.sf ${file}

		# Perform clang-tidy again
		clang-tidy  -fix\
			-checks="readability-braces-around-statements,\
			readability-isolate-declaration" \
			-fix-errors\
			$file -- $FLAGS > /dev/null 2>&1
		set +x
	fi

	# Perform cannotate if haven't run before
	if ! $(grep -Fq "/*** Processed by CAnnotate ***/" $file); then
		echo -e "${GREEN}-> Running ${BGREEN}CAnnotate${NC} on $file..."
		if [ $DEBUG -eq 1 ]; then 
			set -x
			cannotate $file --crit="$CRIT" -o ${file}.tmp.ca -D $DO_COV -n $file -- $FLAGS
		else
			cannotate $file --crit="$CRIT" -o ${file}.tmp.ca $DO_COV -n $file -- $FLAGS > /dev/null
		fi
		# Use mv to get correct number of label when grep
		mv ${file}.tmp.ca ${file}
		set +x
	fi
	echo -e "${GREEN}-> Finishing annotating${NC} ${file} with ${BLUE}CRIT='${CRIT}', COV=${COV}.${NC}"
}

# Function:    check_docannotate()
#    Check if we are annotating a file with WHITELIST or SKIPLIST
#
# GLOBALS:   
#    WHITELIST, SKIPLIST, INCLUDES, FLAGS, GREEN, BLUE, NC
# ARGUMENTS: 
#    $1 file
#
check_do_cannotate() {
	cfile=$1
	f_cfile="$(cd $(dirname $cfile) && pwd)/$(basename $cfile)"
	if [ -n "$WHITELIST" ]; then
		for wfile in ${WHITELIST[@]}; do
			if [[ "$f_cfile" == *"$wfile"* ]]; then
				return 0
			fi
		done
		return 1
	else
		for skipfile in ${SKIPLIST[@]}; do
			if [[ "$f_cfile" == *"$skipfile"* ||
				"$f_cfile" == "/usr/"* ]]; then
				[ $DEBUG -eq 1 ] && echo -e "${GREEN}Skipping${NC} $cfile..."
				return 1
			fi
		done
	fi
}

HELP_MSG="
OVERVIEW: CAnnotate tool wrapper

USAGE:    cannotate-cc [-h|--help|-E|<compiling options>] files...

OPTIONS:
  -h|--help   Display this help messa
  -E          Perform CAnnotate without compiling the files

ENV:
  CRIT        state which coverage criteria to use. Default: all
  ORIG_CC     the original compiler to involve after annotation. Default: gcc
  WHITELIST   the files to perform cannotate, overwrites SKIPLIST
  SKIPLIST    the files directly sent to ORIG_CC without annotation
"

# Extracting arguments
arrVar=()
while [[ $# > 0 ]] ;do
	if [[ $1 == "-DOBJ"*"="* ]] ; then
		p1=$( echo $1 | cut -d'=' -f1 )
		p2=$( echo $1 | cut -d'=' -f2- )
		arg=$(echo $p1=\"$p2\" )
	else
		arg=$(echo $1 | sed 's/ /\\ /g')
	fi
	arrVar[${#arrVar[@]}]=$arg
	case "$arg" in
		-h|--help)
			echo -e "$HELP_MSG"
			exit
			;;
		-o)
			if [ ! -z "$OUTPUT" ]; then
				echo -e "${RED}error:${NC} output filename specified twice"
				exit 1
			fi
			OUTPUT="$1 $2"
			arrVar[${#arrVar[@]}]=$(echo $2 | sed 's/ /\\ /g')
			shift
			;;
		-E)
			NO_COMPILE=1
			;;
		*.c)
			CFILES="$CFILES $arg"
			;;
		*)
			FLAGS="$FLAGS $arg"
			;;
	esac
	shift
done

# Annotating each C file
for cfile in ${CFILES[@]}; do
	check_do_cannotate $cfile && cannotate_file $cfile
done

# After annotation, hand it back to the original compiler
[ $NO_COMPILE -eq 1 ] && exit
set -x
$ORIG_CC "${arrVar[@]}"
