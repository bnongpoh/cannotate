/**
 * @file CAnnotate.h
 * @author Marwan Nour      (marwan.nour@cea.fr)
 * @author Bernard Nongpoh  (bernard.nongpoh@cea.fr)
 * @author Wei-Cheng Wu     (wwu@isi.edu)
 * @date 	Created: 2021-05-11
 * 			Last Modified: 2022-06-21
 *
 * @brief CAnnotate is the clang version of LAnnotate from the LTest Frama-C
 * plugin.
 * @details Usage:
 * 		No coverage data: 	./cannotate --crit=<Coverage Criterion/a>
 * <input_file.c> -o <output_file.c> -- -I <headers> With Cov. data:
 * ./cannotate --crit=<Coverage Criterion/a> <input_file.c> -o <output_file.c>
 * -f <output_coverage_data_file> -- -I <headers>
 *
 */
#include "clang/AST/AST.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/ASTContext.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/ASTConsumers.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"

#include "clang/AST/ParentMapContext.h"

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using namespace clang::driver;
using namespace clang::tooling;
using namespace clang;
using namespace std;

/*Feasible file handling*/
std::ifstream feasibleFileStream;

#define ALLOW_UNARY_AOR 0 // set to 1 to allow labeling for unary (ex: x++)
// Criterions enum
enum Criterion { DC, MCC, ABS, AOR, COR, ROR };

// Command Line Option Category
static llvm::cl::OptionCategory CAnnotateCategory("CAnnotate Options");

// Output file Option
static llvm::cl::opt<std::string>
    outFileName("o", llvm::cl::desc("(Required) Output File Name"),
                llvm::cl::value_desc("filename"),
                llvm::cl::cat(CAnnotateCategory), llvm::cl::ValueRequired,
                llvm::cl::Optional);

// Coverage Criteria Option
llvm::cl::list<Criterion> CovCrit(
    "crit", llvm::cl::desc("(Required) Coverage Criterion"),
    llvm::cl::values(
        clEnumVal(DC, "Decision Coverage (DC)"),
        clEnumVal(MCC, "Multiple Condition Coverage (MCC)"),
        clEnumVal(ABS, "Weak Mutation - Absolute Value Insertion (ABS)"),
        clEnumVal(AOR,
                  "Weak Mutation - Arithmetic Operator Replacement  (AOR)"),
        clEnumVal(COR,
                  "Weak Mutation - Conditional Operator Replacement (COR)"),
        clEnumVal(ROR,
                  "Weak Mutation - Relational Operator Replacement (ROR)")),
    llvm::cl::cat(CAnnotateCategory), llvm::cl::ValueRequired,
    llvm::cl::OneOrMore, llvm::cl::CommaSeparated);

// Allow side effects Option
llvm::cl::opt<bool>
    AllowSideEffects("allowSE",
                     llvm::cl::desc("Allow Side Effects (Default: false)"),
                     llvm::cl::cat(CAnnotateCategory), llvm::cl::init(false));

// Allow infeasible labels
llvm::cl::opt<bool> AllowInfeasibleABS(
    "allowINF", llvm::cl::desc("Allow Infeasible ABS Labels (Default: false)"),
    llvm::cl::cat(CAnnotateCategory), llvm::cl::init(false));

// Allow debug
llvm::cl::opt<bool>
    Debug("D", llvm::cl::desc("Enable Debug Prints (Default: false)"),
          llvm::cl::cat(CAnnotateCategory), llvm::cl::init(false));

// Label Coverage Feedback Option
llvm::cl::opt<std::string>
    CovFile("f", llvm::cl::desc("Coverage Feedback Output File Name"),
            llvm::cl::value_desc("feedback-filename"),
            llvm::cl::cat(CAnnotateCategory), llvm::cl::ValueRequired);

// Label Coverage Feedback Option
llvm::cl::opt<std::string>
    LableName("n", llvm::cl::desc("Extra Name for Label ID"),
            llvm::cl::value_desc("label-name"),
            llvm::cl::cat(CAnnotateCategory), llvm::cl::ValueRequired);

// Label Coverage Feedback Option
llvm::cl::opt<int> LabelID("label-id", llvm::cl::desc("Label ID"),
                           llvm::cl::value_desc("label-id"),
                           llvm::cl::cat(CAnnotateCategory), llvm::cl::init(0));

llvm::cl::opt<std::string> FeasibleFile(
    "feasible", llvm::cl::desc("Feasible File to filter out infeasible labels"),
    llvm::cl::value_desc("feasible-filename"),
    llvm::cl::cat(CAnnotateCategory));

// Label Coverage Feedback Option
llvm::cl::opt<int> startLabelNumber("start-label-number", llvm::cl::desc("This is the starting label number. Required for multiple files annotation"),
                           llvm::cl::value_desc("start-label-number"),
                           llvm::cl::cat(CAnnotateCategory), llvm::cl::init(1));

// label number
static int labelnum = 1; // Pick form argument
static int labeled_locations = 0;

set<int> feasibleLabelSet; // feasible labels loading from file

// Label coverage feedback file access
static bool accessed = false;
