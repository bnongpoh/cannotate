#include "CAnnotate.h"
/**
 * @file CAnnotate.cpp
 * @author Marwan Nour      (marwan.nour@cea.fr)
 * @author Bernard Nongpoh  (bernard.nongpoh@cea.fr)
 * @author Wei-Cheng Wu     (wwu@isi.edu)
 * @date 	Created: 2021-05-11
 * 			Last Modified: 2022-07-26
 */

/**
 * @brief Recursive AST Visitor for Label Annotation
 *
 */


std::vector<std::string> skipFunctions;
bool skipFunc = false;
class CAnnotateTransform : public RecursiveASTVisitor<CAnnotateTransform>
{
private:
  Rewriter &rewriter_;
  ASTContext &context_;

public:
  CAnnotateTransform(Rewriter &rewriter, ASTContext &context)
      : rewriter_(rewriter), context_(context) {}

  /**
   * @brief  Converts an int n to a bool vector for binary representation
   *
   * @param n 	number to be converted
   * @param size 	size of the binary output
   * @return vector<bool>
   */
  vector<bool> intToBinaryVec(int n, int size)
  {
    vector<bool> res = {};
    for (int i = size - 1; i >= 0; i--)
    {
      int k = n >> i;
      if (k & 1)
      {
        res.push_back(true);
      }
      else
      {
        res.push_back(false);
      }
    }
    return res;
  }

  /**
   * @brief Gets string representation of an Stmt object
   *
   * @param stmt
   * @return string
   */
  string stmtToString(Stmt *stmt)
  {
    // LangOptions, output string and raw_string_ostream for outputing
    // statements
    if (!stmt) return "";
    clang::LangOptions lo;
    string out_str;
    llvm::raw_string_ostream outstream(out_str);
    stmt->printPretty(outstream, NULL, PrintingPolicy(lo));

    return out_str;
  }

  /**
   * @brief Wrap stmtToString with "()"
   *
   * @param stmt
   * @return string
   */
  string stmtToStringWrap(Stmt *stmt)
  {
    if (!stmt) return "";
    if (isa<ParenExpr>(stmt)) return stmtToString(stmt);
    return "(" + stmtToString(stmt) + ")";
  }

  bool contains(char *a, char *b)
  {
    if (std::strstr(b, a) != NULL)
    { // Strstr says does b contain a
      return true;
    }
    return false;
  }

 deque<string> _add_precondition(deque<string> member_exprs, Expr * node, bool compare_zero=false)
 {
   string expr = stmtToString(node);
   if (compare_zero) expr = "(" + expr + " >= 0)";
   if (node->isEvaluatable(context_)) {
     if (Debug) cout << "        |x Evaluable: " << expr << endl;
     return member_exprs;
   }
   auto iter = std::find(member_exprs.begin(), member_exprs.end(), expr);
   if (iter != member_exprs.end())
   {
     member_exprs.erase(iter);
   }
   member_exprs.push_front(expr);
   if (Debug) cout << "        |_ Get: " << expr << endl;
   return member_exprs;
 }

  /**
   * @brief Test if all pointer accessed in the labes exist
   *        so that we don't cause segfault
   *        E.g. st->ptr->x, return "st && st->ptr"
   *
   * @param nodes - vector<Expr *>
   * @return std::string
   */
  deque<string> _compute_label_preconditions(deque<string> member_exprs, Expr *node)
  {
    if (isa<BinaryOperator>(node)) // case: x OP y
      {
        BinaryOperator *node_BO = dyn_cast<BinaryOperator>(node);
        // Order matters, handle RHS first
        member_exprs = _compute_label_preconditions(member_exprs, node_BO->getRHS());
        member_exprs = _compute_label_preconditions(member_exprs, node_BO->getLHS());
        return member_exprs;
      }
      else if (isa<UnaryOperator>(node))       // case: !x
      {
        UnaryOperator *node_UO = dyn_cast<UnaryOperator>(node);
        Expr *node_n = node_UO->getSubExpr();
        if (node_UO->getOpcode() == UO_Deref)
        {
          member_exprs = _add_precondition(member_exprs, node_n);
        }
        return _compute_label_preconditions(member_exprs, node_n);
      }
      else if (isa<CastExpr>(node)) {
        CastExpr *node_i = dyn_cast<CastExpr>(node);
        return _compute_label_preconditions(member_exprs, node_i->getSubExpr());
      }
      else if (isa<ParenExpr>(node)) {
        ParenExpr *node_i = dyn_cast<ParenExpr>(node);
        return _compute_label_preconditions(member_exprs, node_i->getSubExpr());
      }
      else if (isa<MemberExpr>(node)) {
        MemberExpr *node_m = dyn_cast<MemberExpr>(node);
        Expr *node_n = node_m->getBase();
        /*
        if (!node_m->isArrow()) expr = "&" + expr;
        member_exprs = _add_precondition(member_exprs, expr);
        */
        if (node_m->isArrow()) member_exprs = _add_precondition(member_exprs, node_n);
        return _compute_label_preconditions(member_exprs, node_n);
      }
      else if (isa<DeclRefExpr>(node))
      {
        return member_exprs;
      }
      else if (isa<ArraySubscriptExpr>(node)) // case: arr[idx]
      {
        ArraySubscriptExpr *ar_node = dyn_cast<ArraySubscriptExpr>(node);
        // For arr[idx], check the existence of arr (getLHS) and idx (getRHS)
        // here i suppose code is using arr[idx] instead of idx[arr]
        // idx has to exist, plus > 0
        member_exprs = _add_precondition(member_exprs, ar_node->getRHS(), true);
        member_exprs = _compute_label_preconditions(member_exprs, ar_node->getRHS());
        // arr has to exist
        member_exprs = _add_precondition(member_exprs, ar_node->getLHS());
        member_exprs = _compute_label_preconditions(member_exprs, ar_node->getLHS());
        return member_exprs;
      }

      if (Debug) cout << "        |x Discard: " << stmtToString(node) << endl;
      return member_exprs;
  }

 string compute_label_preconditions(vector<Expr *> nodes)
 {
   deque<string> results = {};
   std::reverse(nodes.begin(), nodes.end());
   for (auto it = nodes.begin(); it != nodes.end(); ++it)
   {
     Expr * node = *it;
     if (Debug) cout << "      |_ Pre-cond handling: " << stmtToString(node) << endl;
     results = _compute_label_preconditions(results, node);
   }
   // Concat the precoditions with '&&'
   std::string ret = "";
   if (results.size() > 0) {
     auto it = results.begin();
     ret = *it++;
     for (; it != results.end(); ++it) ret += " && " + *it;
   }
   if (Debug)
   {
     if (ret.length() == 0) cout << "    |_ Preconditions: -"  << endl;
     else cout << "    |_ Preconditions: " << ret << endl;
   }
   return ret;
 }

  /**
   * @brief Recursively visits an expression and returns a vector of
   * pair<string, bool> where string is a subexpression (or the expression
   * itself if it doesn't have subexpressions) and bool indicates whether or not
   * the subexpression is to be negated.
   * Also, each expr stored in the pair will be output as a vector of <Expr *>.
   * This is to generate precoditions for the MCC lables chunck.
   *
   * @param expr_list_pair - Output vector<pair<string, bool>>
   * @param expr_list - vector<Expr *>
   * @param node - Current Expr
   * @param iter - Current Iteration
   * @return tuple<vector<pair<string, bool>>, vector<Expr *>>
   */
  tuple<vector<pair<string, bool>>, vector<Expr *>>
  expr_visitor_MCC(vector<pair<string, bool>> expr_list_pair,
                   vector<Expr *> expr_list,
                   Expr *node,
                   int iter)
  {
    if (Debug)
    {
      if (Debug) cout << "  |_ MCC iter: " << to_string(iter) <<
       ", " << stmtToString(node) << endl;
    }
    if (!AllowSideEffects)
    {
      if (node->HasSideEffects(context_))
      {
        if (Debug) cout << "    |x side-effect: " << stmtToString(node) << endl;
        expr_list_pair.push_back({"1", false});
        return std::make_tuple(expr_list_pair, expr_list);
      }
    }
    // Fall through if the condition can be fold into constant boolean value
    if (node->isEvaluatable(context_)) {
      expr_list_pair.push_back({stmtToString(node), false});
      return std::make_tuple(expr_list_pair, expr_list);
    }

    if(isa<ConditionalOperator>(node)){
        ConditionalOperator *condOperator=dyn_cast<ConditionalOperator>(node);
        tie(expr_list_pair, expr_list) =
            expr_visitor_MCC(expr_list_pair, expr_list, condOperator->getCond(), iter);
    }

    else if (isa<BinaryOperator>(node)) // case: x OP y
    {
      BinaryOperator *node_BO = dyn_cast<BinaryOperator>(node);
      if (node_BO->isLogicalOp())
      { // case: x || y, x && y
        // visit lhs
        tie(expr_list_pair, expr_list) =
            expr_visitor_MCC(expr_list_pair, expr_list, node_BO->getLHS(), iter);

        // push &&
        expr_list_pair.push_back({"&&", false});

        // visit rhs
        tie(expr_list_pair, expr_list) =
            expr_visitor_MCC(expr_list_pair, expr_list, node_BO->getRHS(), iter);
      }
      else
      { // case: x == 2, x + y, x < 0
        if ((isa<CallExpr>(node_BO->getLHS()) ||
             isa<CallExpr>(node_BO->getRHS())))
        {
          // push {1, false} if function calls are within a BinaryOperator
          expr_list_pair.push_back({"1", false});
        }
        else
        {
          // push subexpression
          expr_list_pair.push_back({stmtToString(node_BO), true});
          expr_list.push_back(node);

          if (Debug) cout << "    |_ MCC atom: " << stmtToString(node_BO) << endl;
        }
      }
    }
    else if (isa<IntegerLiteral>(node)      // case: 1
             || isa<CastExpr>(node)         // case: (cast) x
             || isa<UnaryOperator>(node)    // case: !x
    )
    {
      expr_list_pair.push_back({stmtToString(node), true});
      expr_list.push_back(node);
      if (Debug) cout << "    |_ MCC atom: " << stmtToString(node) << endl;
    }
    else if (isa<CallExpr>(node)) // case: foo() -> Don't create labels for
                                  // function calls (push {1, false} instead)
    {
      expr_list_pair.push_back({"1", false});
      // return {};
    }
    else if (isa<ParenExpr>(node)) // case: (x)
    {
      ParenExpr *node_paren = dyn_cast<ParenExpr>(node);
      if (Debug) cout << "\tnode_paren_print : " << stmtToString(node_paren) << endl;
      // push token ( before
      expr_list_pair.push_back({"(", false});
      // visit subexpression inside parentheses
      int pre_size = expr_list_pair.size();
      tie(expr_list_pair, expr_list) =
          expr_visitor_MCC(expr_list_pair, expr_list, node_paren->getSubExpr(), iter);
      // push token ) after
      //Hackcode: Need proper testing, if the expr_list_pair remains the same, meaning no
      if(expr_list_pair.size()!=pre_size)
        expr_list_pair.push_back({")", false});
      else
        expr_list_pair.pop_back();
    }

    return std::make_tuple(expr_list_pair, expr_list);
  }

  /**
   * @brief Get the MCC combinations from a given vector of string and a vector
   * of int
   *
   * @param expr_list  vector<string> containing subexpressions of an expression
   * @return vector<string>
   */
  vector<string> get_MCC_combinations(vector<pair<string, bool>> str_vec)
  {
    vector<string> combinations_vec = {};
    // count the number of true values in pos_vec
    int count_pos = 0;
    for (pair<string, bool> x : str_vec)
    {
      if (x.second)
      {
        count_pos++;
      }
    }
    if (count_pos > 10)
    {
      if (Debug) cout << "    |_ MCC skip as too many arguments (" << count_pos << ")\n";
      return {};
    }

    // get truth table size
    int num_combinations = pow(2, count_pos);

    // generate combinations
    for (int i = 0; i < num_combinations; i++)
    {
      // create binary vector
      vector<bool> bin_vec = intToBinaryVec(i, count_pos);

      // build combination string
      string comb = "";
      int pos = 0;

      for (int j = 0; j < (int)str_vec.size(); j++)
      {
        string subexpr = str_vec[j].first;

        if (str_vec[j].second)
        {
          if (bin_vec[pos])
          {
            // negate subexpr
            subexpr.insert(subexpr.begin(), '(');
            subexpr.insert(subexpr.begin(), '!');
            subexpr.insert(subexpr.end(), ')');
          }
          pos++;
        }
        comb += subexpr + " ";
      }
      if (Debug) cout << "    |_ MCC combination #" << i << ": " << comb << endl;
      combinations_vec.push_back(comb);
    }

    return combinations_vec;
  }

  /**
   * @brief Get the variables from an expression (helpful for WM ABS)
   *
   * @param set_vars - output set of variables in string format
   * @param node - current expression
   * @return set<string>
   */
  tuple<set<string>, vector<Expr *>>
  get_vars(set<string> set_vars,
           vector<Expr *> expr_list,
           Expr *node)
  {
    if (!AllowSideEffects)
    {
      if (node->HasSideEffects(context_))
      {
        if (Debug) cout << "    |x side-effect: " << stmtToString(node) << endl;
        return {};
      }
    }

    if (isa<BinaryOperator>(node)) // case: x OP y
    {
      BinaryOperator *node_BO = dyn_cast<BinaryOperator>(node);
      // visit lhs
      tie(set_vars, expr_list) =
        get_vars(set_vars, expr_list, node_BO->getLHS());
      // visit rhs
      tie(set_vars, expr_list) =
        get_vars(set_vars, expr_list, node_BO->getRHS());
    }
    else if (isa<UnaryOperator>(node)) // case: x--, ~x, x++
    {
      UnaryOperator *unary_node = dyn_cast<UnaryOperator>(node);
      if (unary_node->getOpcode() == UO_Not ||
          unary_node->getOpcode() == UO_PreDec ||
          unary_node->getOpcode() == UO_PostDec ||
          unary_node->getOpcode() == UO_PostInc ||
          unary_node->getOpcode() == UO_PreInc)
      {
        tie(set_vars, expr_list) =
          get_vars(set_vars, expr_list, unary_node->getSubExpr());
      }
    }
    else if (isa<ImplicitCastExpr>(node)) // case: x
    {
      ImplicitCastExpr *impcast = dyn_cast<ImplicitCastExpr>(node);
      // Only add labels for L2R values when --allowINF flag is false
      if (AllowInfeasibleABS ||
          (!AllowInfeasibleABS &&
           (strcmp(impcast->getCastKindName(), "LValueToRValue") == 0)))
      {
        // Add string variable to the set
        if (node->getType().getTypePtr()->isArithmeticType()) //
          set_vars.insert(stmtToString(node));
          expr_list.push_back(node);
          if (Debug) cout << "    |_ ABS atom: " << stmtToString(node) << endl;
      }

      Expr *subexpr = impcast->getSubExpr();

      if (isa<ArraySubscriptExpr>(subexpr)) // case: ar[x]
      {
        ArraySubscriptExpr *ar_node = dyn_cast<ArraySubscriptExpr>(subexpr);
        tie(set_vars, expr_list) =
          get_vars(set_vars, expr_list, ar_node->getIdx());
      }
      else
      {
        tie(set_vars, expr_list) =
          get_vars(set_vars, expr_list, subexpr);
      }
    }

    return std::make_tuple(set_vars, expr_list);
  }

  /**
   * @brief Creates labels from Expr * of Criterion (AOR,COR,ROR)
   *          calls binop_visitor_LABEL() in handling different BinaryOperator
   *
   * @param cor_labels - output cor labels
   * @param expr_list - vector to store stmt to compute pre-conditions
   * @param node - Expr *
   * @param label_mode - Different operations for different labels
   *
   * @return tuple<vector<string>, vector<Expr *>>
   */
  tuple<vector<string>, vector<Expr *>>
  expr_visitor_multiple(vector<string> labels,
                 vector<Expr *> expr_list,
                 Expr *node,
                 Criterion label_mode)
  {
    if (!AllowSideEffects)
    {
      if (node->HasSideEffects(context_))
      {
        if (Debug) cout << "    |x side-effect: " << stmtToString(node) << endl;
        return {};
      }
    }
    string label_name = "";
    switch (label_mode) {
      case AOR:
      {
        label_name = "AOR";
        break;
      }
      case COR:
      {
        label_name = "COR";
        break;
      }
      case ROR:
      {
        label_name = "ROR";
        break;
      }
      default:
      {
        throw std::invalid_argument("Unexpected case in expr_visitor_multiple\n");
      }
    }

    if (isa<BinaryOperator>(node))
    {
      switch (label_mode) {
        case AOR:
        {
          tie(labels, expr_list) =
            binop_visitor_AOR(labels, expr_list, node);
            break;
        }
        case COR:
        {
          tie(labels, expr_list) =
            binop_visitor_COR(labels, expr_list, node);
          break;
        }
        case ROR:
        {
          tie(labels, expr_list) =
            binop_visitor_ROR(labels, expr_list, node);
          break;
        }
      }
    }
    else if (isa<UnaryOperator>(node))
    {
      #if ALLOW_UNARY_AOR
      if (label_mode == AOR)
      {
        UnaryOperator *node_unary = dyn_cast<UnaryOperator>(node);
        std::string expr = stmtToStringWrap(node);

        expr_list.push_back(node_unary->getSubExpr());
        if (Debug) cout << "    |_ " << label_name << " atom: " << stmtToString(node) << endl;

        if (node_unary->getOpcode() == UO_PostInc ||
            node_unary->getOpcode() == UO_PreInc)
        {
          labels.push_back(
            expr + " - 1 " +
            " != " +
            expr + " + 1"
          );
        }
        else if (node_unary->getOpcode() == UO_PostDec ||
                 node_unary->getOpcode() == UO_PreDec)
        {
          labels.push_back(
            expr + " + 1 " +
            " != " +
            expr + " - 1"
          );
        }
      }
      # else
      UnaryOperator *unary_node = dyn_cast<UnaryOperator>(node);
      if (unary_node->getOpcode() == UO_Not ||
          unary_node->getOpcode() == UO_PreDec ||
          unary_node->getOpcode() == UO_PostDec ||
          unary_node->getOpcode() == UO_PostInc ||
          unary_node->getOpcode() == UO_PreInc)
      {
        tie(labels, expr_list) =
          expr_visitor_multiple(labels, expr_list, unary_node->getSubExpr(), label_mode);
      }
      #endif
    }
    else if (isa<CallExpr>(node))
    {
      // return empty list if there is a function call
      return {};
    }
    else if (isa<CastExpr>(node))
    {
      CastExpr *castexpr = dyn_cast<CastExpr>(node);
      Expr *subexpr = castexpr->getSubExpr();

      if (isa<ArraySubscriptExpr>(subexpr)) // case: ar[x + y]
      {
        ArraySubscriptExpr *ar_node = dyn_cast<ArraySubscriptExpr>(subexpr);
        tie(labels, expr_list) =
          expr_visitor_multiple(labels, expr_list, ar_node->getIdx(), label_mode);
      }
      else
      {
        tie(labels, expr_list) =
          expr_visitor_multiple(labels, expr_list, subexpr, label_mode);
      }
    }
    else if (isa<ParenExpr>(node)) // case: (x)
    {
      ParenExpr *expr = dyn_cast<ParenExpr>(node);
      tie(labels, expr_list) =
        expr_visitor_multiple(labels, expr_list, expr->getSubExpr(), label_mode);
    }

    return std::make_tuple(labels, expr_list);
  }

  // Reference :
  // https://www-soc.lip6.fr/trac/netbsdtsar/browser/vendor/netbsd/8/src/external/bsd/llvm/dist/clang/include/clang/AST/StmtVisitor.h?rev=298
  /**
   * @brief binop_visitor_AOR() in handling different BinaryOperator
   *
   * @param aor_labels - output cor labels
   * @param expr_list - vector to store stmt to compute pre-conditions
   * @param node - Expr *
   *
   * @return tuple<vector<string>, vector<Expr *>>
   */
  tuple<vector<string>, vector<Expr *>>
  binop_visitor_AOR(vector<string> aor_labels,
                  vector<Expr *> expr_list,
                  Expr *node)
  {
    BinaryOperator *node_BO = dyn_cast<BinaryOperator>(node);
    // Handle opcode
    /*Check if it is a compatible type*
     *
     * If LHS or RHS is a string, skip adding Mutation Coverage.
     * */
    QualType lhsType = node_BO->getLHS()->getType();
    QualType rhsType = node_BO->getRHS()->getType();

    bool isCompatibleType = false;
    bool noAdditionOperator = false;
    bool noSubstractOpeartor = false;

    /* Special Case for various pointer types, where if a and b are this type,
     we may not do + or - conversion to the origianl operator */
    if (lhsType.getCanonicalType().getAsString().find("*") != string::npos ||
        rhsType.getCanonicalType().getAsString().find("*") != string::npos) {
      noAdditionOperator = true;
      noSubstractOpeartor = true;
    }

    // Both RHS and LHS Should be arithmetic types for / and * (My Assumption)
    if (lhsType.getTypePtr()->isArithmeticType() &&
        rhsType.getTypePtr()->isArithmeticType())
    {
      isCompatibleType = true;
    }

    std::string expr = stmtToStringWrap(node);
    std::string lhs = stmtToStringWrap(node_BO->getLHS());
    std::string rhs = stmtToStringWrap(node_BO->getRHS());
    if (Debug) cout << "    |_ AOR atom: " << lhs << endl;
    if (Debug) cout << "    |_ AOR atom: " << rhs << endl;

    expr_list.push_back(node_BO->getLHS());
    expr_list.push_back(node_BO->getRHS());

    if (node_BO->getOpcode() == BO_Mul ||
        node_BO->getOpcode() == BO_MulAssign)
    {
      aor_labels.push_back(
        lhs + " - " + rhs +
        " != " +
        lhs + " * " +rhs
      );
      //if(!noAdditionOperator)
      aor_labels.push_back(
        lhs + " + " + rhs +
        " != " +
        lhs + " * " + rhs
      );
      if (isCompatibleType)
      {
        aor_labels.push_back(
          "(" + rhs + " != 0) " +
          " && (" +
            lhs + " / " + rhs +
            " != " +
            lhs + " * " + rhs +
            ")"
        );
      }
    }
    else if (node_BO->getOpcode() == BO_Div ||
             node_BO->getOpcode() == BO_DivAssign)
    {
      aor_labels.push_back(
        lhs + " - " + rhs +
        " != " +
        lhs + " / " + rhs
      );
      //if(!noAdditionOperator)
      aor_labels.push_back(
        lhs + " + " + rhs +
         " != " +
         lhs + " / " + rhs
       );
      // Remove as it is quite easy to introduce Arithmetic exception
      /*
      if (isCompatibleType)
        aor_labels.push_back(
          lhs + " * " + rhs +
          " != " +
          lhs + " / " + rhs
        );
      */
    }
    else if (node_BO->getOpcode() == BO_Rem ||
             node_BO->getOpcode() == BO_RemAssign)
    {
      aor_labels.push_back(
        lhs + " - " + rhs +
         " != " +
         lhs + " % " + rhs
       );
      if(!noAdditionOperator)
        aor_labels.push_back(
          lhs + " + " + rhs +
          " != " +
          lhs + " % " + rhs
        );
      if (isCompatibleType)
        aor_labels.push_back(
          lhs + " * " + rhs +
          " != " +
          lhs + " % " + rhs
        );
    }
    else if (node_BO->getOpcode() == BO_Add ||
             node_BO->getOpcode() == BO_AddAssign)
    {
      if(!noSubstractOpeartor)
        aor_labels.push_back(
          lhs + " - " + rhs +
          " != " +
          lhs + " + " + rhs
        );
      if (isCompatibleType)
      {
        aor_labels.push_back(
          rhs + "!= 0 " +
          "&& (" + lhs + " / " + rhs +
                  " != " +
                  lhs + " + " + rhs +
              ")"
        );
      }
      if (isCompatibleType)
        aor_labels.push_back(
          lhs + " * " + rhs +
          " != " +
          lhs + " + " + rhs
        );
    }
    else if (node_BO->getOpcode() == BO_Sub ||
             node_BO->getOpcode() == BO_SubAssign)
    {
      if (isCompatibleType)
      {
        aor_labels.push_back(
          rhs + "!= 0 " +
          " && (" + lhs + " / " + rhs +
                    " != " +
                    lhs + " - " + rhs +
              ")"
        );
      }
      if(!noAdditionOperator)
      {
        aor_labels.push_back(
          lhs + " + " + rhs +
          " != " + lhs + " - " + rhs
        );
      }
      if (isCompatibleType)
        aor_labels.push_back(
          lhs + " * " + rhs +
          " != " +
          lhs + " - " + rhs
        );
    }
    else if (node_BO->getOpcode() == BO_And ||
             node_BO->getOpcode() == BO_AndAssign)
    {
      aor_labels.push_back("(" + lhs + " | " +
                           rhs + ")" +
                           " != " + "(" + lhs + " & " +
                           rhs + ")");
      aor_labels.push_back("(" + lhs + " ^ " +
                           rhs + ")" +
                           " != " + "(" + lhs + " & " +
                           rhs + ")");
    }
    else if (node_BO->getOpcode() == BO_Or ||
             node_BO->getOpcode() == BO_OrAssign)
    {
      aor_labels.push_back("(" + lhs + " & " +
                           rhs + ")" +
                           " != " + "(" + lhs + " | " +
                           rhs + ")");
      aor_labels.push_back("(" + lhs + " ^ " +
                           rhs + ")" +
                           " != " + "(" + lhs + " | " +
                           rhs + ")");
    }
    else if (node_BO->getOpcode() == BO_Xor ||
             node_BO->getOpcode() == BO_XorAssign)
    {
      aor_labels.push_back("(" + lhs + " | " +
                           rhs + ")" +
                           " != " + "(" + lhs + " ^ " +
                           rhs + ")");
      aor_labels.push_back("(" + lhs + " & " +
                           rhs + ")" +
                           " != " + "(" + lhs + " ^ " +
                           rhs + ")");
    }
    else if (node_BO->getOpcode() == BO_Shl ||
             node_BO->getOpcode() == BO_ShlAssign)
    {
      aor_labels.push_back("(" + lhs + " >> " +
                           rhs + ")" +
                           " != " + "(" + lhs + " << " +
                           rhs + ")");
    }
    else if (node_BO->getOpcode() == BO_Shr ||
             node_BO->getOpcode() == BO_ShrAssign)
    {
      aor_labels.push_back("(" + lhs + " << " +
                           rhs + ")" +
                           " != " + "(" + lhs + " >> " +
                           rhs + ")");
    }
    // visit lhs
    tie(aor_labels, expr_list) =
      expr_visitor_multiple(aor_labels, expr_list, node_BO->getLHS(), AOR);
    // visit rhs
    tie(aor_labels, expr_list) =
      expr_visitor_multiple(aor_labels, expr_list, node_BO->getRHS(), AOR);

    return std::make_tuple(aor_labels, expr_list);
  }

  /**
   * @brief binop_visitor_COR() in handling different BinaryOperator
   *
   * @param aor_labels - output cor labels
   * @param expr_list - vector to store stmt to compute pre-conditions
   * @param node - Expr *
   *
   * @return tuple<vector<string>, vector<Expr *>>
   */
  tuple<vector<string>, vector<Expr *>>
  binop_visitor_COR(vector<string> cor_labels,
                    vector<Expr *> expr_list,
                    Expr *node)
  {
    BinaryOperator *node_BO = dyn_cast<BinaryOperator>(node);
    std::string expr = stmtToStringWrap(node);
    std::string lhs = stmtToStringWrap(node_BO->getLHS());
    std::string rhs = stmtToStringWrap(node_BO->getRHS());
    if (Debug) cout << "    |_ COR atom: " << lhs << endl;
    if (Debug) cout << "    |_ COR atom: " << rhs << endl;

    expr_list.push_back(node_BO->getLHS());
    expr_list.push_back(node_BO->getRHS());

    switch (node_BO->getOpcode())
    {
    case BO_LOr:
      expr_list.push_back(node_BO->getLHS());
      expr_list.push_back(node_BO->getRHS());
      cor_labels.push_back(
        expr + " != (" + lhs + " && " + rhs + ")"
      );
      break;
    case BO_LAnd:
    {
      expr_list.push_back(node_BO->getLHS());
      expr_list.push_back(node_BO->getRHS());
      if (!node_BO->getLHS()->getType().getTypePtr()->isArithmeticType())
        cor_labels.push_back(
          "(" + lhs + "!= NULL" + ")" +
          " && (" +
            "(" + lhs + " || " + rhs + ") != " + expr +
          ")"
        );
      else {
        cor_labels.push_back(
          expr + " != (" + lhs + " || " + rhs + ")"
        );
      }
      break;
    }
    default:
      if (Debug) cout << "    |_ No COR for this opcode" << endl;
      break;
    }

    // visit lhs
    tie(cor_labels, expr_list) =
      expr_visitor_multiple(cor_labels, expr_list, node_BO->getLHS(), COR);
    // visit rhs
    tie(cor_labels, expr_list) =
      expr_visitor_multiple(cor_labels, expr_list, node_BO->getRHS(), COR);

    return std::make_tuple(cor_labels, expr_list);
  }

  /**
   * @brief binop_visitor_ROR() in handling different BinaryOperator
   *
   * @param aor_labels - output cor labels
   * @param expr_list - vector to store stmt to compute pre-conditions
   * @param node - Expr *
   *
   * @return tuple<vector<string>, vector<Expr *>>
   */
  tuple<vector<string>, vector<Expr *>>
  binop_visitor_ROR(vector<string> ror_labels,
                    vector<Expr *> expr_list,
                    Expr *node)
  {
    BinaryOperator *node_BO = dyn_cast<BinaryOperator>(node);
    std::string expr = stmtToStringWrap(node);
    std::string lhs = stmtToStringWrap(node_BO->getLHS());
    std::string rhs = stmtToStringWrap(node_BO->getRHS());
    if (Debug) cout << "    |_ LOR atom: " << lhs << endl;
    if (Debug) cout << "    |_ ROR atom: " << rhs << endl;

    expr_list.push_back(node_BO->getLHS());
    expr_list.push_back(node_BO->getRHS());

    switch (node_BO->getOpcode())
    {
    case BO_LT:
      ror_labels.push_back(
        "(" + lhs + " <= " + rhs + ") != " + expr
      );
      ror_labels.push_back(
        "(" + lhs + " > " + rhs + ") != " + expr
      );
      ror_labels.push_back(
        "(" + lhs + " >= " + rhs + ") != " + expr
      );
      break;

    case BO_GT:
      ror_labels.push_back(
        "(" + lhs + " <= " + rhs + ") != " + expr
      );
      ror_labels.push_back(
        "(" + lhs + " < " + rhs + ") != " + expr
      );
      ror_labels.push_back(
        "(" + lhs + " >= " + rhs + ") != " + expr
      );
      break;

    case BO_LE:
      ror_labels.push_back(
        "(" + lhs + " < " + rhs + ") != " + expr
      );
      ror_labels.push_back(
        "(" + lhs + " > " + rhs + ") != " + expr
      );
      ror_labels.push_back(
        "(" + lhs + " >= " + rhs + ") != " + expr
        );
      break;

    case BO_GE:
      ror_labels.push_back(
        "(" + lhs + " < " + rhs + ") != " + expr
      );
      ror_labels.push_back(
        "(" + lhs + " > " + rhs + ") != " + expr
      );
      ror_labels.push_back(
        "(" + lhs + " <= " + rhs + ") != " + expr
      );
      break;

    case BO_EQ:
      ror_labels.push_back(
        "(" + lhs + " != " + rhs + ") != " + expr
      );
      break;

    case BO_NE:
      ror_labels.push_back(
        "(" + lhs + " == " + rhs + ") != " + expr
      );
      break;

    default:
      break;
    }
    // visit lhs
    tie(ror_labels, expr_list) =
      expr_visitor_multiple(ror_labels, expr_list, node_BO->getLHS(), ROR);
    // visit rhs
    tie(ror_labels, expr_list) =
      expr_visitor_multiple(ror_labels, expr_list, node_BO->getRHS(), ROR);

    return std::make_tuple(ror_labels, expr_list);
  }

  /**
   * @brief Get the label body as string
   *
   * @param crit - criterion
   * @param label_id - label number
   * @return string
   */
  string get_label_body(string crit, int label_id)
  {
    if (LableName.empty())
    {
      string label_body =
          "{\n__asm__ volatile (\"\"::: \"memory\");\n\treport_label_coverage(\"" +
          crit + "\", " + std::to_string(label_id) + ", \"\"" ");\n}\n";
      return label_body;
    }
    else
    {
      string label_body =
          "{\n__asm__ volatile (\"\"::: \"memory\");\n\treport_label_coverage(\"" +
          crit + "\", " + std::to_string(label_id) + ", \"" + LableName + "\");\n}\n";
      return label_body;
    }
  }

  string get_label_prefix(string label_name, int label_num)
  {
    if (LableName.empty())
      return "\n// " + label_name + " Label "
                    + std::to_string(labelnum) + "\n";
    else
      return "\n// " + label_name + " Label "
                    + LableName + "-" + std::to_string(labelnum) + "\n";

  }

  /**
   * @brief Gets the labels from a condition expression
   *
   * @param cond - condition of type Expr *
   * @return string of labels
   */
  string get_labels(Expr *cond, bool DC_enable, bool MCC_enable,
                    bool ABS_enable, bool AOR_enable, bool COR_enable,
                    bool ROR_enable)
  {
    // Need to have a labelID consistent across

    // Skip if the condition can be fold into constant boolean value
    if (cond->isEvaluatable(context_)) {
      if (Debug) cout << "  |x isEvaluatable: " << stmtToString(cond) << endl;
      return "";
    }

    string labels     = "";
    string label_body = "{ __asm__ volatile (\"\"::: \"memory\"); }\n";
    string cond_str   = stmtToString(cond);

    // Loop over options selected
    for (unsigned int optionIndex = 0; optionIndex != CovCrit.size();
         optionIndex++)
    {
      // DC labels
      if (CovCrit[optionIndex] == 0 && !(cond_str.empty()) &&
          (cond_str.compare("<null expr>") != 0) && DC_enable)
      {
        if (!CovFile.empty())
        {
          label_body = get_label_body("DC", labelnum);
          accessed = true;
        }

        string label_to_add = get_label_prefix("DC", labelnum)
                              + "if(" + cond_str + ") " + label_body;
        if (Debug)
        {
          cout << label_to_add << endl;
        }
        // This code is required to TCE
        if (LabelID != 0)
        {
          if (LabelID == labelnum)
          {
            labels += label_to_add;
          }
        }
        else if (!FeasibleFile.empty())
        {
          auto it = feasibleLabelSet.find(labelnum);

          if (it != feasibleLabelSet.end())
          {
            labels += label_to_add;
          }
        }
        else
        {
          labels += label_to_add;
        }
        labelnum++;

        label_to_add = get_label_prefix("DC", labelnum)
                        + "if (!(" + cond_str + ")) " + label_body;

        // This code is required to TCE
        if (LabelID != 0)
        {
          if (LabelID == labelnum)
          {
            labels += label_to_add;
          }
        }
        else if (!FeasibleFile.empty())
        {
          auto it = feasibleLabelSet.find(labelnum);

          if (it != feasibleLabelSet.end())
          {
            labels += label_to_add;
          }
        }
        else
        {
          labels += label_to_add;
        }

        labelnum++;

        DC_enable = false;
      }

      // MCC labels
      if (CovCrit[optionIndex] == 1 && MCC_enable)
      {
        // Call to custom recursive expression visitor
        if (Debug) cout << "===\n MCC visit: " << stmtToString(cond) << endl;
        vector<pair<string, bool>> expr_list_pair;
        vector<Expr *> expr_list;
        tie(expr_list_pair, expr_list) =
            expr_visitor_MCC({}, {}, cond, 0);

        // Only generate labels for decision with more than 1 atomic condition
        if (expr_list_pair.size() > 1)
        {
          // Get MCC combinations
          vector<string> combinations = get_MCC_combinations(expr_list_pair);

          if (!combinations.empty())
          {
            labels += "\n";
            // Wrap the whole MCC parts with preconditions, if there are ones
            string preconds = compute_label_preconditions(expr_list);
            if (preconds.length() > 0) {
              labels += "// MCC labels preconditions\n";
              labels += "if (" + preconds + ")\n";
            }
            labels += "{\n";

            for (int i = 0; i < (int)combinations.size(); i++)
            {
              if (Debug) cout << "\t\t" << combinations[i] << endl;
              if (!CovFile.empty())
              {
                label_body = get_label_body("MCC", labelnum);
                accessed = true;
              }
              string mcc_label = "if(" + combinations[i] + ") " + label_body;
              string label_to_add = get_label_prefix("MCC", labelnum) + mcc_label;

              // This code is required to TCE
              if (LabelID != 0)
              {
                if (LabelID == labelnum)
                {
                  labels += label_to_add;
                }
              }
              else if (!FeasibleFile.empty())
              {
                auto it = feasibleLabelSet.find(labelnum);
                if (it != feasibleLabelSet.end())
                {
                  labels += label_to_add;
                }
              }
              else
              {
                labels += label_to_add;
              }

              labelnum++;
            }
            labels += "}\n";

            if (Debug) cout << "  |_ MCC labeled!" << endl;
          }
          else
          {
            if (Debug) cout << "  |x MCC <nil>" << endl;
          }
        }
        MCC_enable = false;
      }

      // ABS Labels
      if (CovCrit[optionIndex] == 2 && ABS_enable)
      {
        // Get variables in condition in set
        set<string> vars_set = {};
        vector<Expr *> expr_list = {};

        if (Debug) cout << "===\n ABS get_vars: " << stmtToString(cond) << endl;
        tie(vars_set, expr_list) = get_vars({}, {}, cond);

        if (!vars_set.empty())
        {
          labels += "\n";
          // Wrap the whole parts with preconditions, if there are ones
          string preconds = compute_label_preconditions(expr_list);
          if (preconds.length() > 0) {
            labels += "// ABS labels preconditions\n";
            labels += "if (" + preconds + ")\n";
          }
          labels += "{\n";

          for (string x : vars_set)
          {
            if (!CovFile.empty())
            {
              label_body = get_label_body("ABS", labelnum);
              accessed = true;
            }

            string label_to_add = get_label_prefix("ABS", labelnum)
                                  + "if(" + x + " < 0)  " + label_body;
            // This code is required to TCE
            if (LabelID != 0)
            {
              if (LabelID == labelnum)
              {
                labels += label_to_add;
              }
            }
            else if (!FeasibleFile.empty())
            {
              auto it = feasibleLabelSet.find(labelnum);

              if (it != feasibleLabelSet.end())
              {
                labels += label_to_add;
              }
            }
            else
            {
              labels += label_to_add;
            }

            labelnum++;
          }
          labels += "}\n";
          if (Debug) cout << "  |_ ABS labeled!" << endl;
        }
        else
        {
          if (Debug) cout << "  |x ABS <nil>" << endl;
        }

        ABS_enable = false;
      }

      // AOR, COR, ROR Labels
      if ((CovCrit[optionIndex] == 3 && AOR_enable) ||
          (CovCrit[optionIndex] == 4 && COR_enable) ||
          (CovCrit[optionIndex] == 5 && ROR_enable)
          )
      {
        vector<string> label_strings = {};
        vector<Expr *> expr_list = {};
        string label_name = "";
        Criterion mode;
        switch (CovCrit[optionIndex])
        {
          case 3:
          {
            label_name = "AOR";
            mode = AOR;
            if (Debug) cout << "===\n expr_visitor_AOR: " << stmtToString(cond) << endl;
            break;
          }
          case 4:
          {
            label_name = "COR";
            mode = COR;
            if (Debug) cout << "===\n expr_visitor_COR: " << stmtToString(cond) << endl;
            break;
          }
          case 5:
          {
            label_name = "ROR";
            mode = ROR;
            if (Debug) cout << "===\n expr_visitor_ROR: " << stmtToString(cond) << endl;
            break;
          }
          default:
          {
            throw std::invalid_argument("Unexpected case in get_labels\n");
          }
        }
        tie(label_strings, expr_list) =
            expr_visitor_multiple({}, {}, cond, mode);
        if (!label_strings.empty())
        {
          labels += "\n";
          // Wrap the whole parts with preconditions, if there are ones
          string preconds = compute_label_preconditions(expr_list);
          if (preconds.length() > 0) {
            labels += "// " + label_name + " labels preconditions\n";
            labels += "if (" + preconds + ")\n";
          }
          labels += "{\n";


          for (int i = 0; i < (int)label_strings.size(); i++)
          {
            if (!CovFile.empty())
            {
              label_body = get_label_body(label_name, labelnum);
              accessed = true;
            }
            string label_to_add = get_label_prefix(label_name, labelnum)
                                  + "if (" + label_strings[i] + ")  " + label_body;
            // This code is required to TCE
            if (LabelID != 0)
            {
              if (LabelID == labelnum)
              {
                labels += label_to_add;
              }
            }
            else if (!FeasibleFile.empty())
            {
              auto it = feasibleLabelSet.find(labelnum);

              if (it != feasibleLabelSet.end())
              {
                labels += label_to_add;
              }
            }
            else
            {
              labels += label_to_add;
            }

            labelnum++;
          }
          labels += "}\n";
          if (Debug) cout << "  |_ " << label_name << " labeled!" << endl;
        }
        else
        {
          if (Debug) cout << "  |x " << label_name << " <nil>" << endl;
        }

        switch (CovCrit[optionIndex])
        {
          case 3:
          {
            AOR_enable = false;
            break;
          }
          case 4:
          {
            COR_enable = false;
            break;
          }
          case 5:
          {
            ROR_enable = false;
            break;
          }
        }
      }

    }

    if (labels.length() != 0) labeled_locations++;

    return labels;
  }

  /**
   * @brief ifStmt Visitor
   *
   * @param ifStmt
   * @return true
   */
  bool VisitIfStmt(IfStmt *ifStmt)
  {
    if (skipFunc) return true;
    // Get condition
    Expr *cond = ifStmt->getCond();

    // Get condition as string
    string cond_str = stmtToString(cond);

    if (Debug) cout << "\n--------\nIFSTMT: " << cond_str << endl;

    // Handle case: else if
    if (ifStmt->hasElseStorage() && isa<IfStmt>(ifStmt->getElse()))
    {
      // place the if inside the else body
      rewriter_.ReplaceText(ifStmt->getElseLoc(), "else {");
      rewriter_.InsertTextAfterToken(ifStmt->getElse()->getEndLoc(), ";}\n");
    }

    // Handle generating the same labels multiple times (also trigger wanted
    // labels)
    bool DC_enable = true;
    bool MCC_enable = true;
    bool ABS_enable = true;
    bool AOR_enable = true;
    bool COR_enable = true;
    bool ROR_enable = true;

    // Get labels
    string labels = get_labels(cond, DC_enable, MCC_enable, ABS_enable,
                               AOR_enable, COR_enable, ROR_enable);

    // Write Labels
    rewriter_.InsertTextBefore(ifStmt->getBeginLoc(), labels);

    return true;
  }

  /**
   * @brief WhileStmt Visitor
   *
   * @param whileStmt
   * @return true
   */
  bool VisitWhileStmt(WhileStmt *whileStmt)
  {
    if (skipFunc) return true;
    Expr *cond = whileStmt->getCond();
    string cond_str = stmtToString(cond);

    if (Debug) cout << "\n--------\nWHILESTMT: " << cond_str << endl;


    // Replace while(cond) with while(1)
    // rewriter_.ReplaceText(cond->getSourceRange(), "1");

    // Handle generating the same labels multiple times (also trigger wanted
    // labels)
    bool DC_enable = true;
    bool MCC_enable = true;
    bool ABS_enable = true;
    bool AOR_enable = true;
    bool COR_enable = true;
    bool ROR_enable = true;

    // Get labels
    string labels = get_labels(cond, DC_enable, MCC_enable, ABS_enable,
                               AOR_enable, COR_enable, ROR_enable);

    // // Insert text before while body begin loc
    rewriter_.InsertTextBefore(whileStmt->getBeginLoc(), labels);

    /**** Insert labels inside loop ****/
    // Essentially, insert labels before the condition is checked
    // Skip if stmt doesn't have body
    if (!isa<NullStmt>(whileStmt->getBody()))
    {
      labels = get_labels(cond, DC_enable, MCC_enable, ABS_enable, AOR_enable,
                          COR_enable, ROR_enable);
      // Insert labels before the loop ends
      SourceLocation end_loc = whileStmt->getBody()->getEndLoc();
      rewriter_.InsertTextBefore(end_loc, labels);
    }
    return true;
  }

  /**
   * @brief DoStmt Visitor
   *
   * @param doStmt
   * @return true
   */
  bool VisitDoStmt(DoStmt *doStmt)
  {
    if (skipFunc) return true;

    Expr *cond = doStmt->getCond();

    if (Debug) cout << "\n--------\nDOSTMT: " << stmtToString(cond) << endl;

    // Handle generating the same labels multiple times (also trigger wanted
    // labels)
    bool DC_enable = true;
    bool MCC_enable = true;
    bool ABS_enable = true;
    bool AOR_enable = true;
    bool COR_enable = true;
    bool ROR_enable = true;

    /**** Insert labels at the end of the loop ****/
    // Essentially, insert labels before the condition is checked
    // Skip if stmt doesn't have body
    if (!isa<NullStmt>(doStmt->getBody()))
    {
      string labels = get_labels(cond, DC_enable, MCC_enable, ABS_enable,
                                       AOR_enable, COR_enable, ROR_enable);
      SourceLocation end_loc = doStmt->getBody()->getEndLoc();
      rewriter_.InsertTextBefore(end_loc, labels);
    }
    return true;
  }

  /**
   * @brief ForStmt visitor
   *
   * @param forStmt
   * @return true
   */
  bool VisitForStmt(ForStmt *forStmt)
  {
    if (skipFunc) return true;

    Expr *cond = forStmt->getCond();
    Expr *inc = forStmt->getInc();
    Stmt *init = forStmt->getInit();

    if (Debug) cout <<
    "\n--------\nFORSTMT: {"
    << stmtToString(init) << "; "
    << stmtToString(cond) << "; "
    << stmtToString(inc) << "}" << endl;

    // Handle generating the same labels multiple times (also trigger wanted
    // labels)
    bool DC_enable = true;
    bool MCC_enable = true;
    bool ABS_enable = true;
    bool AOR_enable = true;
    bool COR_enable = true;
    bool ROR_enable = true;

    string labels = "";

    // Get labels from cond
    if (cond != nullptr)
    {
      // TODO: Wei-Cheng
      //    Since I remove handling side-effect handling of for-loop
      if (AllowSideEffects || !cond->HasSideEffects(context_))
      {
        labels += get_labels(cond, DC_enable, MCC_enable, ABS_enable, AOR_enable,
                             COR_enable, ROR_enable);
      }
      else if (Debug) cout << "    |x side-effect: " << stmtToString(cond) << endl;
    }
    // Get labels from inc
    if (inc != nullptr)
    {
      labels +=
          get_labels(inc, false, false, ABS_enable, AOR_enable, false, false);
    }

    rewriter_.InsertTextBefore(forStmt->getBeginLoc(), labels);

    if (init != nullptr)
    {
      SourceManager &SM = rewriter_.getSourceMgr();
      if (!isa<DeclStmt>(
              init))
      { // case: int i; for(i = 0; ...)	-> need to add ; to init
        rewriter_.InsertTextBefore(forStmt->getBeginLoc(),
                                   stmtToString(init) + ";\n");
        // Using getExpansionRange to include the macros in defination
        rewriter_.ReplaceText(SM.getExpansionRange(init->getSourceRange()), "");
      }
      else
      { // case: for(int i = 0; ...)	-> need to add ; to replaced
        // init. Also wrapped with brackets so that we don't re-declared
        // the init variable
        rewriter_.InsertTextBefore(forStmt->getBeginLoc(),
                                   "{\n" + stmtToString(init) + "\n");
        rewriter_.ReplaceText(SM.getExpansionRange(init->getSourceRange()), ";");
        rewriter_.InsertTextAfter(forStmt->getEndLoc(), "};\n");
      }
    }

    return true;
  }

  /**
   * @brief ReturnStmt visitor
   *
   * @param retStmt
   * @return true
   */
  bool VisitReturnStmt(ReturnStmt *retStmt)
  {
    if (skipFunc) return true;

    if (retStmt->getRetValue() != nullptr)
    {

      Expr *retVal = retStmt->getRetValue();
      if (Debug) cout << "\n--------\nRETSTMT: " << stmtToString(retVal) << endl;
      bool DC_enable = true;
      bool MCC_enable = true;
      bool ABS_enable = true;
      bool AOR_enable = true;
      bool COR_enable = true;
      bool ROR_enable = true;

      string labels = get_labels(retVal, DC_enable, MCC_enable, ABS_enable,
                                 AOR_enable, COR_enable, ROR_enable);

      rewriter_.InsertTextBefore(retStmt->getReturnLoc(), labels);
    }

    return true;
  }

  /**
   * @brief CompoundStmt visitor
   *
   * @param compStmt
   * @return true
   */
  bool VisitCompoundStmt(CompoundStmt *compStmt)
  {
    if (skipFunc) return true;

    for (Stmt *stmt : compStmt->body())
    {
      if (Debug) cout << "\n--------\nCompund: " << stmtToString(stmt) << endl;
      // Handle generating the same labels multiple times (also trigger wanted
      // labels)
      bool ABS_enable = true;
      bool AOR_enable = true;
      bool COR_enable = true;
      bool ROR_enable = true;

      if (isa<UnaryOperator>(stmt)) // case: x++, !x, ~x, x--
      {
        UnaryOperator *UOP = dyn_cast<UnaryOperator>(stmt);

        string labels = get_labels(UOP, false, false, ABS_enable, AOR_enable,
                                   COR_enable, ROR_enable);
        rewriter_.InsertTextBefore(UOP->getBeginLoc(), labels);
      }
      else if (isa<BinaryOperator>(stmt)) // case: x OP y
      {
        BinaryOperator *BO = dyn_cast<BinaryOperator>(stmt);

        string labels = get_labels(BO, false, false, ABS_enable, AOR_enable,
                                   COR_enable, ROR_enable);
        rewriter_.InsertTextBefore(BO->getBeginLoc(), labels);
      }
      else if (isa<DeclStmt>(stmt)) // case: int x = y OP z
      {
        DeclStmt *dstmt = dyn_cast<DeclStmt>(stmt);
        // Dependency Analysis here/.
        for (Decl *x : dstmt->decls())
        {
          if (isa<VarDecl>(x))
          {
            VarDecl *vdecl = dyn_cast<VarDecl>(x);
            if (vdecl->hasInit())
            {
              Expr *vdecl_init = vdecl->getInit();

              string labels = get_labels(vdecl_init, false, false, ABS_enable,
                                         AOR_enable, COR_enable, ROR_enable);
              rewriter_.InsertTextBefore(dstmt->getBeginLoc(), labels);
            }
          }
        }
      }
      else if (isa<CallExpr>(stmt)) // case: foo(x + y || z) -> Generate
                                    // labels for function arguments
      {
        CallExpr *callExpr = dyn_cast<CallExpr>(stmt);
        Expr **call_args = callExpr->getArgs();
        int num_args = callExpr->getNumArgs();

        string labels = "";
        for (int i = 0; i < num_args; i++)
        {
          labels += get_labels(call_args[i], false, false, ABS_enable,
                               AOR_enable, COR_enable, ROR_enable);
        }

        rewriter_.InsertTextBefore(callExpr->getBeginLoc(), labels);
      }
    }

    return true;
  }

  /**
   * @brief FunctionDecl visitor. Inserts coverage reporting code to main
   * function
   *
   * @param funDecl
   * @return true
   */
  bool VisitFunctionDecl(FunctionDecl *funcDecl)
  {
    std:string func_name = funcDecl->getNameAsString();
    for (std::string s : skipFunctions) {
      if (func_name.compare(s) == 0) {
        skipFunc = true;
        break;
      } else {
        skipFunc = false;
      }
    }
    return true;
  }
};

class CAnnotateASTConsumer : public ASTConsumer
{
private:
  Rewriter &rewriter_;

public:
  CAnnotateASTConsumer(Rewriter &rewriter) : rewriter_(rewriter) {}
  virtual void HandleTranslationUnit(ASTContext &Context) override
  {
    TranslationUnitDecl *TU = Context.getTranslationUnitDecl();
    CAnnotateTransform transform(rewriter_, Context);
    transform.TraverseDecl(TU);
  }
};

class CAnnotateAction : public ASTFrontendAction
{
private:
  Rewriter rewriter_;

public:
  CAnnotateAction() {}
  std::unique_ptr<ASTConsumer> CreateASTConsumer(CompilerInstance &CI,
                                                 StringRef file) override
  {
    rewriter_.setSourceMgr(CI.getSourceManager(), CI.getLangOpts());

    if (!FeasibleFile.empty())
    {

      feasibleFileStream.open(FeasibleFile);
      if (!feasibleFileStream)
      {
        llvm::errs() << "Error opening file: " << FeasibleFile << "\n";
      }
      else
      {
        std::string line;
        while (getline(feasibleFileStream, line))
        {
          // Output the text from the file
          feasibleLabelSet.insert(stoi(line));
        }
        feasibleFileStream.close();
      }
    }

    return std::make_unique<CAnnotateASTConsumer>(rewriter_);
  }

  void EndSourceFileAction() override
  {

    std::error_code EC;
    llvm::sys::fs::OpenFlags flags = llvm::sys::fs::OF_Text;
    llvm::raw_fd_ostream FileStream(outFileName, EC, flags);
    if (outFileName.size() != 0)
    {
      if (EC)
      {
        llvm::errs() << "Error: could not write to " << EC.message() << "\n";
      }
      else
      {
        // Add needed headers/comments to beginning of file
        string cov = "";
        // Handle generating the same labels multiple times
        bool DC_enable = true;
        bool MCC_enable = true;
        bool ABS_enable = true;
        bool AOR_enable = true;
        bool COR_enable = true;
        bool ROR_enable = true;

        for (unsigned int optionIndex = 0; optionIndex != CovCrit.size();
             optionIndex++)
        {
          if (CovCrit[optionIndex] == 0 && DC_enable)
          {
            cov += "DC ";
            DC_enable = false;
          }
          if (CovCrit[optionIndex] == 1 && MCC_enable)
          {
            cov += "MCC ";
            MCC_enable = false;
          }
          if (CovCrit[optionIndex] == 2 && ABS_enable)
          {
            cov += "ABS ";
            ABS_enable = false;
          }
          if (CovCrit[optionIndex] == 3 && AOR_enable)
          {
            cov += "AOR ";
            AOR_enable = false;
          }
          if (CovCrit[optionIndex] == 4 && COR_enable)
          {
            cov += "COR ";
            COR_enable = false;
          }
          if (CovCrit[optionIndex] == 5 && ROR_enable)
          {
            cov += "ROR ";
            ROR_enable = false;
          }
        }
        labelnum--;
        FileStream 	<< "/*** Processed by CAnnotate ***/ \n"
        			<< "// COVERAGE CRITERIA : " << cov << "\n";

        if (!CovFile.empty())
        {
          // insert report_label_coverage declaration
           FileStream << "#include \"ca_coverage.h\"\n";
        }
        FileStream << "#ifndef __GNUC__\n"
              << "#define __asm__ asm\n"
              << "#endif\n" ;
        int label_generated = labelnum - startLabelNumber.getValue() + 1;
        cout << "COMPLETED"<< endl;
        cout << "LABELS GENERATED: " << label_generated << endl;
        cout << "LABELED LOCATIONS: " << labeled_locations << endl;

        FileStream << "/*** \n" << "CAnnotate LABELS GENERATED: "
                   << std::to_string(label_generated) << "\n";
        FileStream << "CAnnotate LABELED LOCATIONS: "
                   << std::to_string(labeled_locations) << "\n***/\n";

        SourceManager &SM = rewriter_.getSourceMgr();

        rewriter_.getEditBuffer(SM.getMainFileID()).write(FileStream);

      }
    }
    else
    {
      cout << "COMPLETED: Check the source file"<< endl;
      cout << "NextLabelID:" << ++labelnum << endl;
      rewriter_.overwriteChangedFiles();
    }
  }
};

int main(int argc, const char **argv)
{
  auto ExpectedParser =

      CommonOptionsParser::create(argc, argv, CAnnotateCategory);

  if (!ExpectedParser)
  {
    // Fail gracefully for unsupported options.
    return 1;
  }

  if(const char* env_s = std::getenv("SKIPFUNCS")) {
    std::string s( env_s ), tmp;
    std::stringstream ss(s);
    while (std::getline(ss, tmp, ' ')) {
      skipFunctions.push_back(tmp);
    }
  }

  labelnum = startLabelNumber.getValue(); // Get the start labelID

  CommonOptionsParser &OptionsParser = ExpectedParser.get();

  ClangTool Tool(OptionsParser.getCompilations(),

                 OptionsParser.getSourcePathList());

  return Tool.run(newFrontendActionFactory<CAnnotateAction>().get());
}
