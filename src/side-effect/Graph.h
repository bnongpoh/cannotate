/*
@Author: Bernard Nongpoh
@Email: bernard.nongpoh@cea.fr
*/

#include <bits/stdc++.h>
#include "clang/AST/AST.h"
using namespace std;
using namespace clang;



class Graph{
    public:
        map<int,Expr*> exprMap;
        const int thenNodeID = -2;
        const int elseNodeID = -3;
        bool hasElse;

    Graph(bool hasElse);
    map<int,map<bool,int>> adjMap;

    void addEdge(int src, int dest, bool path);
    int getRootId();
    int size();
    set<int> getNodesMultipleInDegree();
     int addExpr(Expr *expr);
    void printGraph();
};
