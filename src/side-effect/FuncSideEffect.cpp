/**
 * @author Bernard Nongpoh (bernard.nongpoh@cea.fr)
 * @author Wei-Cheng Wu (wwu@isi.edu)
 */

#include "Graph.h"
#include "clang/AST/AST.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/ASTContext.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Analysis/BodyFarm.h"
#include "clang/Frontend/ASTConsumers.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"

using namespace clang::driver;
using namespace clang::tooling;
using namespace clang;
using namespace std;

Rewriter rewriter;
const int thenNodeID = -2;
const int elseNodeID = -3;

int labelCounter = 0;
set<int64_t> visitedNodeIds; // This set store the IDs of IfStmt, to avoid
                             // further processing for nested if.

vector<LabelDecl *> continue_label_decls;

static llvm::cl::OptionCategory
    FuncSideEffectTool("Side Effect Tool options \n"
    "Usage: \n"
    "./side-effect input.c -o output.c\n"
    "./side-effect input.c # Transforming directly on the input.c \n"
    "./side-effect input.c -- -I /path/to/library1/ -I /path/to/library2 # if input.c depends on other libraries \n"
    );

static llvm::cl::opt<std::string>
    outFileName("o", llvm::cl::desc("Output File Name (Optional)"),
                llvm::cl::value_desc("filename"),
                llvm::cl::cat(FuncSideEffectTool), llvm::cl::ValueRequired,
                llvm::cl::Optional);

// Allow debug
static llvm::cl::opt<bool>
    Debug("D", llvm::cl::desc("Enable Debug Prints (Default: false)"),
          llvm::cl::cat(FuncSideEffectTool), llvm::cl::init(false));

static std::vector<std::string> skipFunctions;

class FuncSideEffectClassVisitor
    : public RecursiveASTVisitor<FuncSideEffectClassVisitor> {

private:
  ASTContext *Context;

public:
  explicit FuncSideEffectClassVisitor(ASTContext *Context) : Context(Context) {

    rewriter.setSourceMgr(Context->getSourceManager(), Context->getLangOpts());
  }

  // Change the order of traversal
  bool shouldTraversePostOrder() { return false; }

  bool isLeafBinaryOperator(BinaryOperator *bo) {

    return (!isa<BinaryOperator>(bo->getLHS()) &&
            !isa<BinaryOperator>(bo->getRHS()));
  }

  // Debug function for graph that prints the exprs
  void printGraph(Graph *sg) {
    int rootID = sg->getRootId();
    if (rootID > 0)
      cout << "  + Root: " << stmtToString(sg->exprMap[rootID]) << endl;
    else if (rootID == 0)
      cout << "  + Root: nullptr" << endl;
    else
      cout << "  + Error finding root" << endl;

    for (auto x: sg->adjMap) {
      string one = "";
      string two = "x";
      string three = "x";
      if (x.first > 0) one = stmtToString(sg->exprMap[x.first]);
      if (x.second[true] != 0) {
        if (x.second[true] > 0) two = stmtToString(sg->exprMap[x.second[true]]);
        else if (x.second[true] == thenNodeID) two = "then stmt";
        else two = "else stmt";
      }
      if (x.second[false] != 0) {
        if (x.second[false] > 0) three = stmtToString(sg->exprMap[x.second[false]]);
        else if (x.second[false] == thenNodeID) three = "then stmt";
        else three = "else stmt";
      }
      if (one.length() == 0) continue;
      cout << "  |_ " << one << " t-> " << two << endl;
      cout << "  |_ " << one << " f-> " << three << endl;
    }
    cout << endl;
  }

  /**
   * @brief  Recursive function to construct a graph from the condition expression
   *         BO_LAnd  +- lhs +- T -> evaluate rhs
   *                  |      |_ F -> do F
   *                  |_ rhs +- determine T/F
   *         BO_Or    +- lhs +- T -> do T
   *                  |      |_ F -> evaluate rhs
   *                  |_ rhs +- determine T/F
   * @param (Expr *expr, Graph *graph, pair<int, int> next)
   *        next contains the node id when <goto_true, goto_false>
   * @return  triple<int root, int goto_true, int goto_false>
   */
  tuple<int, int, int> constructCondGraph(Expr *expr, Graph *graph, pair<int, int> next)
  {
    if (!isSplittingRequired(expr)) {
      // Add as node
      int id = graph->addExpr(expr); // Map Expr to Node in the Graph.
      graph->addEdge(id, next.first, true);
      if (next.second == elseNodeID) {
        if (graph->hasElse) graph->addEdge(id, next.second, false);
      } else {
        graph->addEdge(id, next.second, false);
      }
      return make_tuple(id, next.first, next.second);
    }
    // TODO: Wei-Cheng:
    //      Not operator !(expr) is not splitting, also in isSplittingRequired
    if (isa<BinaryOperator>(expr)) {
      BinaryOperator *binOp = dyn_cast<BinaryOperator>(expr);
      if (binOp->isLogicalOp()) {
        // Always visit rhs first
        tuple<int, int, int> rhs = constructCondGraph(binOp->getRHS(), graph, next);
        int rhs_root = std::get<0>(rhs);
        int rhs_true = std::get<1>(rhs);
        int rhs_false = std::get<2>(rhs);

        if (binOp->getOpcode() == BO_LAnd) {
          pair<int, int> next_for_lhs = make_pair(rhs_root, next.second);
          return constructCondGraph(binOp->getLHS(), graph, next_for_lhs);

        } else if (binOp->getOpcode() == BO_LOr) {
          pair<int, int> next_for_lhs = make_pair(next.first, rhs_root);
          return constructCondGraph(binOp->getLHS(), graph, next_for_lhs);

        }
      }
      // The else branch should not exists as it should be handled in isSplittingRequired
      string msg = "Un-handled binOp in constructCondGraph for " + stmtToString(expr);
      throw std::invalid_argument( msg );

    } else if (isa<ParenExpr>(expr)) {
      ParenExpr *parenExpr = dyn_cast<ParenExpr>(expr);
      return constructCondGraph(parenExpr->getSubExpr(), graph, next);
    }

    string msg = "Un-handled case in constructCondGraph for " + stmtToString(expr);
    throw std::invalid_argument( msg );
  }


  string stmtToString(Stmt *stmt) {
    if (!stmt) return "";
    clang::LangOptions lo;
    string out_str;
    llvm::raw_string_ostream outstream(out_str);
    stmt->printPretty(outstream, NULL, PrintingPolicy(lo));
    return out_str;
  }

  int tempCounter;

  bool isAtomicBinaryOperator(Expr *expr) {

    if (isa<BinaryOperator>(expr)) {
      BinaryOperator *binOp = dyn_cast<BinaryOperator>(expr);
      if (isa<BinaryOperator>(binOp->getLHS()))
        return false;
      if (isa<BinaryOperator>(binOp->getRHS()))
        return false;
    }

    return true; // TODO
  }

  // SideEffects
  bool isSplittingRequired(Expr *expr) {

    if (!expr->HasSideEffects((*Context))) {
      if (Debug) cout << "    - !isSplittingRequired - no SideEffects: "
        << stmtToString(expr) << endl;
      return false;
    }

    if (isa<ParenExpr>(expr)) {
      ParenExpr *parenExpr = dyn_cast<ParenExpr>(expr);
      return isSplittingRequired(parenExpr->getSubExpr());
    }

    // TODO: Wei-Cheng:
    //      Not operator !(expr) is not splitting.
    if (isa<BinaryOperator>(expr)) {

      BinaryOperator *binOp = dyn_cast<BinaryOperator>(expr);
      if (!binOp->isLogicalOp()) {
        if (Debug) cout << "    - !isSplittingRequired - not isLogicalOp: "
          << stmtToString(expr) << endl;
        return false; // Split only for logical
      }
      // Check side effect for both left and right.
      if (binOp->getRHS()->HasSideEffects((*Context)))
        return true;
      return binOp->getLHS()->HasSideEffects((*Context));
    }
    if (Debug) cout << "    - !isSplittingRequired - any other reason: "
      << stmtToString(expr) << endl;
    return false;
  }


/*We should other VisitCXX since FunctionDecl already handled it*/
  bool VisitFunctionDecl(FunctionDecl *funcDecl) {

    std:string func_name = funcDecl->getNameAsString();

    for (std::string s : skipFunctions) {
      if (func_name.compare(s) == 0) return true;
    }

    tempCounter = 0;
    if (visitedNodeIds.count(funcDecl->getID())
       ||
       (funcDecl->getPreviousDecl()!=nullptr && visitedNodeIds.count(funcDecl->getPreviousDecl()->getID())))
       return true;

    visitedNodeIds.insert(funcDecl->getID());

    if (funcDecl->hasBody() && !visitedNodeIds.count(funcDecl->getBody()->getID((*Context)))) {
      if (Debug) cout << endl << "+++ FUNC: " << func_name << endl;
      Stmt *funcBody = getTransformStmt(funcDecl->getBody());
      if (funcBody!=nullptr)
        rewriter.ReplaceText(funcBody->getSourceRange(), stmtToString(funcBody));
      visitedNodeIds.insert(funcDecl->getBody()->getID((*Context)));
    }
    return true;
  }

  Stmt *constructIfStmtWithGraph(int nodeId, Graph *graph, IfStmt *ifStmt,
                  set<int> requiredLabelNodes,
                  map<int, LabelDecl *> &labelDeclMap, set<int> &visitedNodes) {

    if (nodeId == 0)
      return nullptr;

    if (visitedNodes.count(nodeId)) {
      // Return a GotoStmt from the target node to this node
      GotoStmt *gotoStmt = new (Context) GotoStmt(
          labelDeclMap[nodeId], ifStmt->getBeginLoc(), ifStmt->getEndLoc());
      return gotoStmt;
    }

    visitedNodes.insert(nodeId); // Marked as visited

    if (nodeId == thenNodeID || nodeId == elseNodeID) {
      // Leaf Node, either Then or Else Part, check nodeId for Then or Else

      // Observation: Then Block always required a label, but anyway we are
      // checking from Graph
      if (requiredLabelNodes.count(nodeId)) {

        visitedNodeIds.insert(ifStmt->getID((*Context)));

        IdentifierInfo *Name = nullptr;
        labelCounter++;
        string label =
            "label_" +
            to_string(
                labelCounter); // FIXME, Make label counter function scope.
        Name = &(*Context).Idents.get(
            label); // FIXME: We need to calculate the label ID here, Restart
                    // label_1, If program have identifier label_*, we may have
                    // problems. :) Not a priority now//

        LabelDecl *labelDecl =
            LabelDecl::Create((*Context), Context->getTranslationUnitDecl(),
                              ifStmt->getBeginLoc(), Name);

        labelDeclMap.insert(pair<int, LabelDecl *>(nodeId, labelDecl));

        // Ref to use from:
        // https://rev.ng/gitlab/or1k/clang/-/blob/a3899eb5e30426b00b80232a15ae557dd4caa5b8/lib/Sema/SemaExpr.cpp

        LabelStmt *labelStmt = nullptr;
        if (nodeId == thenNodeID) {
          // Handle ThenNode

          labelStmt = new (Context)
              LabelStmt(ifStmt->getBeginLoc(), labelDecl, ifStmt->getThen());

        } else if (nodeId == elseNodeID) {

          labelStmt = new (Context)
              LabelStmt(ifStmt->getBeginLoc(), labelDecl, ifStmt->getElse());

        } else {
          labelStmt =
              new (Context) LabelStmt(ifStmt->getBeginLoc(), labelDecl, 0);
        }

        // Handle Other Block Statement

        return labelStmt;
      } else {
        // Return Node Without labelling. Since In-degree is < 2
        if (nodeId == thenNodeID)
          return ifStmt->getThen();
        if (nodeId == elseNodeID)
          return ifStmt->getElse();
      }
    }

    Stmt *fStmt = constructIfStmtWithGraph(graph->adjMap[nodeId][false], graph, ifStmt,
                            requiredLabelNodes, labelDeclMap, visitedNodes);

    Stmt *tStmt = constructIfStmtWithGraph(graph->adjMap[nodeId][true], graph, ifStmt,
                            requiredLabelNodes, labelDeclMap, visitedNodes);

    Expr *cond = graph->exprMap[nodeId];

    // cout<<"Here-->"<<nodeId;

    //  return nullptr;

    Stmt *genIfStmt = getIfStmtWithOutSideEffect(cond, tStmt, fStmt);

    visitedNodeIds.insert(
        genIfStmt->getID((*Context))); // We marked it as visited so that we
                                       // will not visit it again.
    // Compound Statement Here
    // If cond contains CallExpr, add the var dec here

    // Get genIFStmt here

    // Check if the node required label and should be present in labelDecMap

    if (requiredLabelNodes.count(nodeId)) {

      if (!labelDeclMap.count(nodeId)) { // Should be in the map

        IdentifierInfo *Name = nullptr;
        labelCounter++;
        string label =
            "label_" +
            to_string(
                labelCounter); // FIXME, Make label counter function scope.
        Name = &(*Context).Idents.get(label);
        LabelDecl *labelDecl = LabelDecl::Create(
            (*Context), Context->getTranslationUnitDecl(),
            genIfStmt->getBeginLoc(), Name); // FIXME: move to function
        // Make sure, we did insert before
        LabelStmt *labelStmt = new (Context)
            LabelStmt(genIfStmt->getBeginLoc(), labelDecl, genIfStmt);
        labelDeclMap.insert(pair<int, LabelDecl *>(nodeId, labelDecl));

        return labelStmt;
      }
    }

    // When We create IfStatement, basically We are adding node to the AST. The
    // AST Visitor will also try to visit the newly create Node, hence we have
    // segmentation fault.

    return genIfStmt;
  }

  // This function remove side effect on condition of an if statement
  Stmt *getIfStmtWithOutSideEffect(Expr *cond, Stmt *tStmt, Stmt *fStmt) {

    Stmt *genIfStmt = nullptr;

    Stmt *ifStmtPart = nullptr;

    /*if (!cond->HasSideEffects((*Context))) {

      genIfStmt =
          IfStmt::Create((*Context), cond->getBeginLoc(), false, nullptr,
                         nullptr, cond, cond->getBeginLoc(), cond->getEndLoc(),
                         tStmt, cond->getBeginLoc(), fStmt);

      visitedNodeIds.insert(
          genIfStmt->getID((*Context))); // Marked as Visited Node

      return genIfStmt;
    }*/

    vector<DeclStmt *> declStmts;

    DeclRefExpr *declRefExpr = searchAndReplaceSideEffects(cond, declStmts);

    // Return a Decl if an expression is of if(foo()) else nullPtr
    if (declRefExpr != nullptr) {
      ifStmtPart =
          IfStmt::Create((*Context), cond->getBeginLoc(), false, nullptr,
                         nullptr, declRefExpr, cond->getBeginLoc(),
                         cond->getEndLoc(), tStmt, cond->getBeginLoc(), fStmt);

    } else {

      ifStmtPart =
          IfStmt::Create((*Context), cond->getBeginLoc(), false, nullptr,
                         nullptr, cond, cond->getBeginLoc(), cond->getEndLoc(),
                         tStmt, cond->getBeginLoc(), fStmt);

      //ifStmtPart->dumpColor();

    }

    visitedNodeIds.insert(ifStmtPart->getID((*Context)));
    // Push declStmt in
    vector<Stmt *> statements;
    // vector<Stmt*> statements{declStmts,stmt1};

    for (DeclStmt *dec : declStmts) {


      visitedNodeIds.insert(dec->getID((*Context)));
      statements.push_back(dec);
    }

    statements.push_back(ifStmtPart);

    genIfStmt = CompoundStmt::Create((*Context), statements,
                                     cond->getBeginLoc(), cond->getEndLoc());

    visitedNodeIds.insert(genIfStmt->getID((*Context)));

    // Insert in a list

    return genIfStmt;
  }

  Stmt *getTransformCompoundStmt(Stmt *stmt) {

    CompoundStmt *compoundStmt = dyn_cast<CompoundStmt>(stmt);

    vector<Stmt *> statements;

    for (CompoundStmt::body_iterator body = compoundStmt->body_begin(),
                                     bodyEnd = compoundStmt->body_end();
         body != bodyEnd; body++) {
      Stmt *bodyStmt = dyn_cast<Stmt>(*body);
      Stmt *newBodyStmt = getTransformStmt(bodyStmt);
        visitedNodeIds.insert(newBodyStmt->getID((*Context)));
      statements.push_back(newBodyStmt);
    }

    CompoundStmt *newCompoundStatement = CompoundStmt::Create(
        (*Context), statements, stmt->getBeginLoc(), stmt->getEndLoc());

    visitedNodeIds.insert(newCompoundStatement->getID((*Context)));
    return newCompoundStatement;
  }

  Stmt *getTransformCaseStmt(Stmt *stmt){

    visitedNodeIds.insert(stmt->getID((*Context)));
    CaseStmt *caseStmt =dyn_cast<CaseStmt>(stmt);
    Stmt *subStmt = getTransformStmt(caseStmt->getSubStmt());
    caseStmt->setSubStmt(subStmt);
    return caseStmt;

  }

  Stmt *getTransformIfStmt(Stmt *stmt) {

    // Marked as Visited Node, This is required to disable traveral by
    // RecursiveVisitor
    visitedNodeIds.insert(stmt->getID((*Context)));

    IfStmt *ifStmt = dyn_cast<IfStmt>(stmt);

    Expr *cond = ifStmt->getCond();
    if (Debug) cout << "--+ IF: " << stmtToString(cond) << endl;
    if (cond==nullptr || !cond->HasSideEffects((*Context))) {
      if (Debug) cout << "  + No need to transform IF: " << stmtToString(cond) << endl;

      Stmt *thenStmt = getTransformStmt(ifStmt->getThen());
      Stmt *elseStmt = getTransformStmt(ifStmt->getElse());
      // Stmt *genIfStmt = getIfStmtWithOutSideEffect(cond, thenStmt, elseStmt);
      ifStmt->setThen(thenStmt);
      if (ifStmt->hasElseStorage())
        ifStmt->setElse(elseStmt);
      // return genIfStmt;
      return ifStmt;
    }

    if (!isSplittingRequired(cond)) {
      if (Debug) cout << "  + No need to split " << stmtToString(cond) << endl;

      Stmt *thenStmt = getTransformStmt(ifStmt->getThen());
      Stmt *elseStmt = getTransformStmt(ifStmt->getElse());
      // Can we simply modify the ifStmt directly
      Stmt *genIfStmt = getIfStmtWithOutSideEffect(cond, thenStmt, elseStmt);

      return genIfStmt;
    }

    // From here, we need splitting
    // Check if condition having single condition statement.
    Stmt *thenPart = getTransformStmt(ifStmt->getThen());
    ifStmt->setThen(thenPart);

    bool hasElse = false;
    if (ifStmt->hasElseStorage()) {
      Stmt *elsePart = getTransformStmt(ifStmt->getElse());
      ifStmt->setElse(elsePart);
      hasElse = true;
    }

    Graph *sg = new Graph(hasElse);
    if (Debug) cout << "  + Constructing cond graph ..." << stmtToString(cond) << endl;
    constructCondGraph(cond, sg, make_pair(thenNodeID, elseNodeID));

    if (Debug) printGraph(sg);

    set<int> requiredLabelNodes = sg->getNodesMultipleInDegree();

    if (Debug)
    {
      cout << "  + Required label nodes: \n\t";
      for (auto x: requiredLabelNodes) if (x > 0) cout << "  |_ " << stmtToString(sg->exprMap[x]) << ", ";
      cout << endl << endl;
    }

    set<int> visitedNodes;

    map<int, LabelDecl *> labelDeclMap;

    Stmt *fStmt = constructIfStmtWithGraph(sg->getRootId(), sg, ifStmt, requiredLabelNodes,
                            labelDeclMap, visitedNodes);
    delete sg;

    return fStmt;
  }
  /*
  Stmt *getTransformDoStmt(Stmt *stmt) {

    visitedNodeIds.insert(stmt->getID((*Context)));

    DoStmt *doStmt = dyn_cast<DoStmt>(stmt);

    Expr *cond = doStmt->getCond();

    if (cond==nullptr || !cond->HasSideEffects((*Context))){
      Stmt *bodyStmt=getTransformStmt(doStmt->getBody());
      doStmt->setBody(bodyStmt);
      return doStmt;
    }


    IfStmt *ifStmt =
        IfStmt::Create((*Context), cond->getBeginLoc(), false, nullptr, nullptr,
                       cond, cond->getBeginLoc(), cond->getEndLoc(),
                       doStmt->getBody(), cond->getBeginLoc(), nullptr);

    IdentifierInfo *Name = nullptr;
    labelCounter++;
    string label =
        "label_" +
        to_string(labelCounter); // FIXME, Make label counter function scope.
    Name = &(*Context).Idents.get(
        label); // FIXME: We need to calculate the label ID here, Restart
                // label_1, If program have identifier label_*, we may have
                // problems. :) Not a priority now//

    LabelDecl *labelDecl =
        LabelDecl::Create((*Context), Context->getTranslationUnitDecl(),
                          ifStmt->getBeginLoc(), Name);

    GotoStmt *gotoStmt = new (Context)
        GotoStmt(labelDecl, ifStmt->getBeginLoc(), ifStmt->getEndLoc());
    vector<Stmt *> bodyStatements;
    Stmt *bodyStmt = getTransformStmt(doStmt->getBody());
    bodyStatements.push_back(bodyStmt);
    bodyStatements.push_back(gotoStmt);

    Stmt *genIfStmt = CompoundStmt::Create(
        (*Context), bodyStatements, stmt->getBeginLoc(), stmt->getEndLoc());

    Stmt *ifStmtNoSideEffect =
        getIfStmtWithOutSideEffect(cond, genIfStmt, nullptr);

    LabelStmt *labelStmt = new (Context)
        LabelStmt(stmt->getBeginLoc(), labelDecl, ifStmtNoSideEffect);

    return labelStmt;
  }

  Stmt *getTransformWhileStmtWithOutSideEffectOnly(Stmt *stmt) {

    visitedNodeIds.insert(stmt->getID((*Context)));

    WhileStmt *whileStmt = dyn_cast<WhileStmt>(stmt);

    Expr *cond = whileStmt->getCond();

    //if (!cond->HasSideEffects((*Context)))
      Stmt *bodyStmt = getTransformStmt(whileStmt->getBody());
        whileStmt->setBody(bodyStmt);
      return whileStmt;
  }*/

  Stmt *getTransformDoWhileStmtWithWhileOne(Stmt *stmt) {

    visitedNodeIds.insert(stmt->getID((*Context)));


    DoStmt *doStmt = dyn_cast<DoStmt>(stmt);

    Expr *cond = doStmt->getCond();

    if (cond==nullptr || !cond->HasSideEffects((*Context)))
    {
      if (Debug) cout << "  + No need to transform DO: " << stmtToString(cond) << endl;
      continue_label_decls.push_back(nullptr);
      Stmt *bodyStmt = getTransformStmt(doStmt->getBody());
        doStmt->setBody(bodyStmt);
      return doStmt;
    }


    ParenExpr *parenExpr =
        new (Context) ParenExpr(cond->getBeginLoc(), cond->getEndLoc(), cond);
    UnaryOperator *notOp = makeNotUnaryOp(parenExpr, cond->getType());
    BreakStmt *breakStmt = new (*Context) BreakStmt(doStmt->getBeginLoc());
    /*Terminating Condition for While*/
    IfStmt *terIfStmt =
        IfStmt::Create((*Context), cond->getBeginLoc(), false, nullptr, nullptr,
                       notOp, cond->getBeginLoc(), cond->getEndLoc(), breakStmt,
                       cond->getBeginLoc(), nullptr);

   /*Process if there is a short ckt*/
   Stmt *ifBreakStmt = getTransformStmt(terIfStmt);
    /***/
    LabelStmt *contLabelStmt = nullptr;
    // Create LabelDecl of inc for conintue to goto
    labelCounter++;
    string label = "label_" + to_string(labelCounter);
    // This label is to covert continue to goto inside this loop
    IdentifierInfo *Name = nullptr;
    Name = &(*Context).Idents.get(label);
    LabelDecl *labelDecl = LabelDecl::Create((*Context),
      Context->getTranslationUnitDecl(), stmt->getEndLoc(), Name);
    continue_label_decls.push_back(labelDecl);

    contLabelStmt = new (Context) LabelStmt(stmt->getEndLoc(), labelDecl, ifBreakStmt);
    if (Debug) cout << "  + Creating " << label
      << " for DO-breakStmt " << stmtToString(breakStmt) << endl;


    Stmt *bodyStmt = getTransformStmt(doStmt->getBody());
    // Make Compound Stmt
    /**
     *  while(1){
     *    body
     *    // continue
    *     goto label_id;
    *     label_id:
     *    if((!x)) break;
     *  }
     */

    vector<Stmt *> statements;
    statements.push_back(bodyStmt);
    statements.push_back(contLabelStmt);


    Stmt *comStmt = CompoundStmt::Create(
        (*Context), statements, stmt->getBeginLoc(), stmt->getEndLoc());

    IntegerLiteral *one = makeIntegerLiteral(1, cond->getType());
    doStmt->setCond(one);
    doStmt->setBody(comStmt);

    return doStmt;
  }

  Stmt *getTransformWhileStmtWithWhileOne(Stmt *stmt) {

    visitedNodeIds.insert(stmt->getID((*Context)));

    WhileStmt *whileStmt = dyn_cast<WhileStmt>(stmt);

    Expr *cond = whileStmt->getCond();

    if (cond==nullptr || !cond->HasSideEffects((*Context))){
      // Explore the body or else the printer will writes to a wrong location or line in the program
        if (Debug) cout << "  + No need to transform WHILE: " << stmtToString(cond) << endl;
        Stmt *bodyStmt = getTransformStmt(whileStmt->getBody());
        whileStmt->setBody(bodyStmt);
        return whileStmt;
    }


    ParenExpr *parenExpr =
        new (Context) ParenExpr(cond->getBeginLoc(), cond->getEndLoc(), cond);
    UnaryOperator *notOp = makeNotUnaryOp(parenExpr, cond->getType());
    BreakStmt *breakStmt = new (*Context) BreakStmt(whileStmt->getBeginLoc());
    /*Terminating Condition for While*/
    IfStmt *terIfStmt =
        IfStmt::Create((*Context), cond->getBeginLoc(), false, nullptr, nullptr,
                       notOp, cond->getBeginLoc(), cond->getEndLoc(), breakStmt,
                       cond->getBeginLoc(), nullptr);

    /***/

    /*Process if there is a short ckt*/
    Stmt *ifBreakStmt = getTransformStmt(terIfStmt);

    Stmt *bodyStmt = getTransformStmt(whileStmt->getBody());
    // Make Compound Stmt
    /**
     *  while(1){
     *    if((!x)) break;
     *    body
     *  }
     *
     */

    vector<Stmt *> statements;
    statements.push_back(ifBreakStmt);
    statements.push_back(bodyStmt);

    Stmt *comStmt = CompoundStmt::Create(
        (*Context), statements, stmt->getBeginLoc(), stmt->getEndLoc());

    IntegerLiteral *one = makeIntegerLiteral(1, cond->getType());
    whileStmt->setCond(one);
    whileStmt->setBody(comStmt);

    return whileStmt;
  }

  Stmt *getTransformForStmtWithWhileOne(Stmt *stmt) {

    visitedNodeIds.insert(stmt->getID((*Context)));


    ForStmt *forStmt = dyn_cast<ForStmt>(stmt);
    Expr *cond = forStmt->getCond();
    Stmt *initStmt = forStmt->getInit();

    if (Debug) cout << "--+ FOR: " << stmtToString(cond) << endl;

    // TODO: Wei-Cheng
    //       Temporarily just remove as this may introduce infinite loops
    if (cond==nullptr || !cond->HasSideEffects((*Context)))
      // Explore the body here
    {
      if (Debug) cout << "  + No need to transform FOR" << endl;
      continue_label_decls.push_back(nullptr);
      Stmt *bodyStmt=getTransformStmt(forStmt->getBody());
      forStmt->setBody(bodyStmt);
      return forStmt;
    }

    ParenExpr *parenExpr =
        new (Context) ParenExpr(cond->getBeginLoc(), cond->getEndLoc(), cond);
    UnaryOperator *notOp = makeNotUnaryOp(parenExpr, cond->getType());
    BreakStmt *breakStmt = new (*Context) BreakStmt(forStmt->getBeginLoc());
    /*Terminating Condition for While*/
    IfStmt *terIfStmt =
        IfStmt::Create((*Context), cond->getBeginLoc(), false, nullptr, nullptr,
                       notOp, cond->getBeginLoc(), cond->getEndLoc(), breakStmt,
                       cond->getBeginLoc(), nullptr);

    /***/
    LabelStmt *contLabelStmt = nullptr;
    if (forStmt->getInc() != nullptr) {
      // Create LabelDecl of inc for conintue to goto
      labelCounter++;
      string label = "label_" + to_string(labelCounter);
      // This label is to covert continue to goto inside this loop
      IdentifierInfo *Name = nullptr;
      Name = &(*Context).Idents.get(label);
      LabelDecl *labelDecl = LabelDecl::Create((*Context),
        Context->getTranslationUnitDecl(), stmt->getEndLoc(), Name);
      continue_label_decls.push_back(labelDecl);

      contLabelStmt = new (Context) LabelStmt(stmt->getEndLoc(), labelDecl, forStmt->getInc());
      if (Debug) cout << "  + Creating " << label
        << " for forInc: " << stmtToString(forStmt->getInc()) << endl;
    } else {
      if (Debug) cout << "  + No for->getInc() label create" << endl;
      continue_label_decls.push_back(nullptr);
    }

    /*Process if there is a short ckt*/
    Stmt *ifBreakStmt = getTransformStmt(terIfStmt);

    Stmt *bodyStmt = getTransformStmt(forStmt->getBody());
    // Make Compound Stmt
    /**
     *  while(1){
     *    if((!x)) break;
     *    // continue
    *     goto label_id;
     *    body
     *    label_id:
     *      for->getInc();
     *  }
     */
    vector<Stmt *> insideWhileLoop;
    insideWhileLoop.push_back(ifBreakStmt);
    insideWhileLoop.push_back(bodyStmt);
    //insideWhileLoop.push_back(forStmt->getInc());
    if (forStmt->getInc() != nullptr) {
      insideWhileLoop.push_back(contLabelStmt);
    }

    Stmt *comStmt1 = CompoundStmt::Create(
        (*Context), insideWhileLoop, stmt->getBeginLoc(), stmt->getEndLoc());

    IntegerLiteral *one = makeIntegerLiteral(1, cond->getType()); // Bugs here, sometime type are of while (1i8)

    WhileStmt *whileStmt = WhileStmt::Create(
        (*Context), nullptr, one, comStmt1, stmt->getBeginLoc(),
        stmt->getBeginLoc(), stmt->getBeginLoc());

    vector<Stmt *> statements;

    if(initStmt!=nullptr)
      statements.push_back(initStmt);

    statements.push_back(whileStmt);

    Stmt *comStmt2 = CompoundStmt::Create(
        (*Context), statements, stmt->getBeginLoc(), stmt->getEndLoc());

    return comStmt2;
  }

  Stmt *getTransformSwitchStmt(Stmt *stmt) {

    SwitchStmt *switchStmt = dyn_cast<SwitchStmt>(stmt);
    Stmt *body = getTransformStmt(switchStmt->getBody());

    switchStmt->setBody(body);
    return switchStmt;
  }

  Stmt *getTransformContinueStmt(Stmt *stmt) {
    if (continue_label_decls.size() == 0) {
      string msg = "No goto label created for CONTINUE, shouldn't happen ";
      throw std::invalid_argument( msg );
    }
    if (continue_label_decls.back() != nullptr) {
      // If cur continue label is not null,
      // jump to the lable instead of executing continue
      GotoStmt *gotoStmt = new (Context) GotoStmt(
        continue_label_decls.back(), stmt->getBeginLoc(), stmt->getEndLoc());
      return gotoStmt;
    } else {
      return stmt;
    }
  }

  // LEGACY: Wei-Cheng: I have no idea why keeping this code
  /*
  Stmt *getTransformForStmt(Stmt *stmt) {

    // Marked as visited Node
    visitedNodeIds.insert(stmt->getID((*Context)));
    return stmt;
    ForStmt *forStmt = dyn_cast<ForStmt>(stmt);
    Expr *cond = forStmt->getCond();

    if (cond==nullptr || !cond->HasSideEffects((*Context))) {

      Stmt *bodyStmt=getTransformStmt(forStmt->getBody());
      forStmt->setBody(bodyStmt);

      return bodyStmt;
    }

    Stmt *initStmt = forStmt->getInit();

    Expr *inc = forStmt->getInc();

    IfStmt *ifStmt =
        IfStmt::Create((*Context), cond->getBeginLoc(), false, nullptr, nullptr,
                       cond, cond->getBeginLoc(), cond->getEndLoc(),
                       forStmt->getBody(), cond->getBeginLoc(), nullptr);

    IdentifierInfo *Name = nullptr;
    labelCounter++;
    string label =
        "label_" +
        to_string(labelCounter); // FIXME, Make label counter function scope.
    Name = &(*Context).Idents.get(
        label); // FIXME: We need to calculate the label ID here, Restart
                // label_1, If program have identifier label_*, we may have
                // problems. :) Not a priority now//

    LabelDecl *labelDecl =
        LabelDecl::Create((*Context), Context->getTranslationUnitDecl(),
                          forStmt->getBeginLoc(), Name);

    GotoStmt *gotoStmt = new (Context)
        GotoStmt(labelDecl, ifStmt->getBeginLoc(), ifStmt->getEndLoc());
    Stmt *bodyStmt = getTransformStmt(forStmt->getBody());
    vector<Stmt *> bodyStatements;
    bodyStatements.push_back(bodyStmt);
    bodyStatements.push_back(inc);
    bodyStatements.push_back(gotoStmt);

    Stmt *genIfStmt = CompoundStmt::Create(
        (*Context), bodyStatements, cond->getBeginLoc(), cond->getEndLoc());

    Stmt *ifStmtNoSideEffect =
        getIfStmtWithOutSideEffect(cond, genIfStmt, nullptr);

    LabelStmt *labelStmt = new (Context)
        LabelStmt(ifStmt->getBeginLoc(), labelDecl, ifStmtNoSideEffect);

    vector<Stmt *> statements;
    statements.push_back(initStmt);
    statements.push_back(labelStmt);

    Stmt *genComp = CompoundStmt::Create(
        (*Context), statements, forStmt->getBeginLoc(), forStmt->getEndLoc());

    return genComp;
  }
  */

  /* A Function return a side-effect free statement (IfStmt, WhileStmt, ForStmt
   * etc) */
  Stmt *getTransformStmt(Stmt *stmt) {

    if (stmt == nullptr || visitedNodeIds.count(stmt->getID((*Context)))) {
      return stmt;
    }
    Stmt *r_stmt = nullptr;

    if(isa<CaseStmt>(stmt)){
      r_stmt = getTransformCaseStmt(stmt);
    }
    else if (isa<IfStmt>(stmt)) {
      r_stmt = getTransformIfStmt(stmt);
    }
    else if (isa<ForStmt>(stmt)) {
      r_stmt = getTransformForStmtWithWhileOne(stmt);
      continue_label_decls.pop_back();
    }
    else if (isa<WhileStmt>(stmt)) {
      // For while loop, we don't have to covert continue
      //   as the break condition check is at the beginning
      // So a hacky way is to push
      // a nullptr to the continue_label_decls stack
      continue_label_decls.push_back(nullptr);
      r_stmt = getTransformWhileStmtWithWhileOne(stmt);
      continue_label_decls.pop_back();
    }
    else if (isa<SwitchStmt>(stmt)) {
      r_stmt = getTransformSwitchStmt(stmt);
    }
    else if (isa<CompoundStmt>(stmt)) {
      r_stmt = getTransformCompoundStmt(stmt);
    }
    else if (isa<DoStmt>(stmt)) {
      // TODO
      r_stmt = getTransformDoWhileStmtWithWhileOne(stmt);
      continue_label_decls.pop_back();
    }
    else if (isa<ContinueStmt>(stmt)) {
      r_stmt = getTransformContinueStmt(stmt);
    }

    if (r_stmt != nullptr) {
      visitedNodeIds.insert(r_stmt->getID((*Context)));
      return r_stmt;
    }
    visitedNodeIds.insert(stmt->getID((*Context)));
    return stmt;
  }

  /*
    This function accept an expression Expr (A Condition Expr), an empty stmts
    (to collect all declared staments) Input: Condition Expr & Vector of
    DeclStmt (To Collect all declared Statement) Output: A vector of DeclStmt
    pass as reference and return new modified condition expr
  */

  DeclRefExpr *getDeclRefExpr(Expr *expr, vector<DeclStmt *> &stmts) {

    QualType type;

    if (isa<UnaryOperator>(expr)) {

      UnaryOperator *unaryOperator = dyn_cast<UnaryOperator>(expr);

      type = unaryOperator->getType();
    }

    if (isa<CallExpr>(expr)) {

      CallExpr *callExpr = dyn_cast<CallExpr>(expr);

      type = callExpr->getCallReturnType((*Context));
    }

    IdentifierInfo *Name = nullptr;
    tempCounter++;
    string label =
        "temp_" +
        to_string(tempCounter); // FIXME, Make label counter function scope.
    Name = &(*Context).Idents.get(label);

    // Create (ASTContext &C, DeclContext *DC, SourceLocation StartLoc,
    // SourceLocation IdLoc, IdentifierInfo *Id, QualType T, TypeSourceInfo
    // *TInfo, StorageClass S)
    VarDecl *varDecl = VarDecl::Create(
        (*Context), Context->getTranslationUnitDecl(), expr->getBeginLoc(),
        expr->getBeginLoc(), Name, type, nullptr, StorageClass::SC_None);

    //  DeclGroupRef 	Create (ASTContext &C, Decl **Decls, unsigned NumDecls)
    // We need DeclStmt to wrap VarDecl
    varDecl->setInit(expr); // This initialized the variable to the call Expr

    DeclGroupRef *declGroupRef = new (Context) DeclGroupRef(varDecl);

    //    DeclGroupRef **declGroupRefs = new DeclGroupRef*[1];
    //  declGroupRefs[0]=declGroupRef;

    // declGroupRef->dump();
    //  	DeclStmt (DeclGroupRef dg, SourceLocation startLoc,
    //  SourceLocation endLoc)
    DeclStmt *declStmt = new (Context)
        DeclStmt(*declGroupRef, expr->getBeginLoc(), expr->getEndLoc());

    visitedNodeIds.insert(declStmt->getID((*Context)));

    stmts.push_back(declStmt); // Insert the declare statment

    // Collect DeclStmt in a list and then  insert it the vector statements

    // Modify the condition here.
    DeclRefExpr *defRefExpr = makeDeclRefExpr(varDecl, false);

    return defRefExpr;
  }

  DeclRefExpr *searchAndReplaceSideEffects(Expr *expr, vector<DeclStmt *> &stmts) {

     if (!expr->HasSideEffects((*Context))) {
       if (Debug) cout << "    - !Replace - no SideEffects: "
         << stmtToString(expr) << endl;
       return nullptr;
     }
    if (isa<UnaryOperator>(expr)) {
      if (Debug) cout << "    - Replacing with decl: " << stmtToString(expr) << endl;
      DeclRefExpr *declRefExpr = getDeclRefExpr(expr, stmts);
      return declRefExpr;
      // UnaryOperator *unaryExpr = dyn_cast<UnaryOperator>(expr);
      // return  searchAndReplaceSideEffects(unaryExpr->getSubExpr(), stmts);
    }

    if (isa<CallExpr>(expr)) {
      if (Debug) cout << "    - Replacing with decl: " << stmtToString(expr) << endl;
      DeclRefExpr *declRefExpr = getDeclRefExpr(expr, stmts);
      return declRefExpr;
    }
    if (isa<BinaryOperator>(expr)) {

      BinaryOperator *binOp = dyn_cast<BinaryOperator>(expr);

      DeclRefExpr *declRefExprLHS =
          searchAndReplaceSideEffects(binOp->getLHS(), stmts);

      if (declRefExprLHS != nullptr) {

        binOp->setLHS(declRefExprLHS);
        //return declRefExprLHS; // We cannot return otherwise we are exploring only LHS
      }
      DeclRefExpr *declRefExprRHS =
          searchAndReplaceSideEffects(binOp->getRHS(), stmts);

      if (declRefExprRHS != nullptr) {

        binOp->setRHS(declRefExprRHS);
        //return declRefExprRHS;
      }

     return nullptr; // FIX ME;
    }
    if (isa<ParenExpr>(expr)) {

      ParenExpr *parenExpr = dyn_cast<ParenExpr>(expr);

      return searchAndReplaceSideEffects(parenExpr->getSubExpr(), stmts);
    }
    if (isa<CastExpr>(expr)) {
      CastExpr *castExpr = dyn_cast<CastExpr>(expr);
      return searchAndReplaceSideEffects(castExpr->getSubExpr(), stmts);
    }

    return nullptr;
  }

  /*Copy from clang/lib/Analysis/BodyFarm.cpp*/
  DeclRefExpr *makeDeclRefExpr(const VarDecl *D,
                               bool RefersToEnclosingVariableOrCapture) {
    QualType Type = D->getType().getNonReferenceType();

    DeclRefExpr *DR = DeclRefExpr::Create(
        (*Context), NestedNameSpecifierLoc(), SourceLocation(),
        const_cast<VarDecl *>(D), RefersToEnclosingVariableOrCapture,
        SourceLocation(), Type, VK_LValue);

    return DR;
  }

  UnaryOperator *makeNotUnaryOp(const Expr *Arg, QualType Ty) {

    return UnaryOperator::Create(((*Context)), const_cast<Expr *>(Arg), UO_LNot,
                                 Ty, VK_LValue, OK_Ordinary, SourceLocation(),
                                 /*CanOverflow*/ false, FPOptionsOverride());
  }

  // Todo:  Remove QualType Ty param
  IntegerLiteral *makeIntegerLiteral(uint64_t Value, QualType Ty) {
    llvm::APInt APValue = llvm::APInt(((*Context)).getTypeSize((*Context).IntTy), Value);
    return IntegerLiteral::Create(((*Context)), APValue, (*Context).IntTy, SourceLocation());
  }
  /*Input: Expr Type
    Return:
      - true: if any sub-expression contains function call (CallExpr Type)
      - false: otherwise
  */
  bool isCondExprContainsSideEffect(Expr *expr) {

    if (isa<CallExpr>(expr)) {
      return true;
    }
    if (isa<UnaryOperator>(expr))
      return true;

    if (isa<BinaryOperator>(expr)) {

      BinaryOperator *binOp = dyn_cast<BinaryOperator>(expr);
      bool res1 = isCondExprContainsSideEffect(binOp->getRHS());
      if (res1)
        return true;
      bool res2 = isCondExprContainsSideEffect(binOp->getLHS());
      return res2;
    }
    if (isa<ParenExpr>(expr)) {
      ParenExpr *parenExpr = dyn_cast<ParenExpr>(expr);
      return isCondExprContainsSideEffect(parenExpr->getSubExpr());
    }

    return false;
  }
};

class FuncSideEffectClassConsumer : public clang::ASTConsumer {
public:
  explicit FuncSideEffectClassConsumer(ASTContext *Context)
      : Visitor(Context) {}

  virtual void HandleTranslationUnit(clang::ASTContext &Context) {
    Visitor.TraverseDecl(Context.getTranslationUnitDecl());
  }

private:
  FuncSideEffectClassVisitor Visitor;
};

class FuncSideEffectClassAction : public clang::ASTFrontendAction {
public:
  virtual std::unique_ptr<clang::ASTConsumer>
  CreateASTConsumer(clang::CompilerInstance &Compiler, llvm::StringRef InFile) {
    rewriter.setSourceMgr(Compiler.getSourceManager(), Compiler.getLangOpts());
    return std::make_unique<FuncSideEffectClassConsumer>(
        &Compiler.getASTContext());
  }

  void EndSourceFileAction() override {

    cout << "\n ✅ Succesfully Transformed the program \n";
    //cout << "\n ✅ Transformed file is in 📁 " << outFileName << " directory \n";
     SourceManager &SM = rewriter.getSourceMgr();
     if(outFileName.size()!=0){
        std::error_code EC;
    llvm::sys::fs::OpenFlags flags = llvm::sys::fs::OF_Text;
    llvm::raw_fd_ostream FileStream(outFileName, EC, flags);
    if (EC) {
      cout << "Error: could not write to " << EC.message() << "\n";
    }
       FileStream << "/*** Processed by Side-Effect Tool ***/\n";
       rewriter.getEditBuffer(SM.getMainFileID()).write(FileStream);

     }
    else {

      rewriter.overwriteChangedFiles(); // Modify the input file directly.

    }
  }
};

int main(int argc, const char **argv) {

  auto ExpectedParser =
      CommonOptionsParser::create(argc, argv, FuncSideEffectTool);
  if (!ExpectedParser) {
    // Fail gracefully for unsupported options.

    return 1;
  }
  if(const char* env_s = std::getenv("SKIPFUNCS")) {
    std::string s( env_s ), tmp;
    std::stringstream ss(s);
    while (std::getline(ss, tmp, ' ')) {
      skipFunctions.push_back(tmp);
    }
  }

  CommonOptionsParser &OptionsParser = ExpectedParser.get();
  ClangTool Tool(OptionsParser.getCompilations(),
                 OptionsParser.getSourcePathList());
  return Tool.run(newFrontendActionFactory<FuncSideEffectClassAction>().get());
}
