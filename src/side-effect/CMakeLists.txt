set(LLVM_LINK_COMPONENTS
  Support
  )
  

add_executable(side-effect FuncSideEffect.cpp Graph.cpp Graph.h)

target_link_libraries(side-effect
  PRIVATE
  clangAST
  clangBasic
  clangFrontend
  clangSerialization
  clangTooling
  clangAnalysis
  )