/*
@Author: Bernard Nongpoh
@Email: bernard.nongpoh@cea.fr
*/

#include"Graph.h"

void Graph::addEdge(int src, int dest, bool path){
      adjMap[src][path]=dest;
      
}

Graph::Graph(bool hasElse){
        
        this->hasElse=hasElse;
        adjMap[thenNodeID][true]=0;
        adjMap[thenNodeID][false]=0;
        if(hasElse)
       { adjMap[elseNodeID][false]=0;
        adjMap[elseNodeID][true]=0;
       }
}



int Graph::getRootId(){

            // 
            int rootId;
            for(auto node:adjMap){

                bool isRoot = true;
                for(auto adjNode:adjMap){
                    if(node.first==adjNode.first) continue;

                    if(node.first==adjNode.second[true]) isRoot=false; // Found Path, not root break and continue with next one
                    if(node.first==adjNode.second[false]) isRoot=false;   

                }

                if(isRoot) return node.first;



                // Found Root 
               
            }
return -1; // No root
}

int Graph::size(){
    return adjMap.size();
}


/*Add Expr into the Map and return its ID*/
int Graph::addExpr(Expr *expr){
    int ID = exprMap.size()+1;
    exprMap[ID]=expr;
    return ID;
}



set<int> Graph::getNodesMultipleInDegree(){
        set<int> nodes;
        for(auto m:adjMap){
            int count = 0;
            for(auto n:adjMap){
                if(m.first==n.first) continue;
                if(m.first==n.second[false]) count++;
                if(m.first==n.second[true]) count++;
                // Count for then and else 
                

            }

            if(count>1){
                nodes.insert(m.first);
            }

        }

    return nodes;
}


void Graph::printGraph(){
        
        for(auto x: adjMap){
                
                cout<<x.first<<"->"<<x.second[true]<<"\n";
                cout<<x.first<<"->"<<x.second[false] <<"\n";


        }
    }

