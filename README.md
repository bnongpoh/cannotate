# CAnnotate Tool for Automated insertion of labels 

CAnnotate is a source code instrumentation tool. The goal is to support a fine-grained coverage metric out-of-the-box (without changing the fuzzer), by transforming the code of the program under test.

## Publication
[Fine-Grained Coverage-Based Fuzzing (FUZZING 2022)](https://www.ndss-symposium.org/ndss-paper/auto-draft-267/)

## Installation
Please find the details in [INSTALL.md](./INSTALL.md).

We also provide a Dockerfile to test out CAnnotate easily! Please run the following commands access it:
```
# Build the docker image
./build_docker.sh

# Start a container
docker run -ti --rm --name "cannoate_demo" cannotate-demo bash
```

## Usage: Compiler Wrapper
We provided a script to easily integrate CAnnotate into any projects. Simply add the following lines to your `Makefile`:
```
export ORIG_CC=$CC      # set ORIG_CC to the compiler you'll be using after annotaion
export CC=cannotate-cc  
```
This will automatically instrument every ".c" files that are going to be compiled, then proceed with the original compiler you set.

Some environment variables can be set to configure CAnnotate:
```
# Set to insert only partial labels (Default: all labels)
CRIT="ABS,AOR,COR,ROR"

# Set WHITELIST to instrument ONLY these files (setting this will ignore SKIPLIST)
export WHITELIST="foo.c bar.c" 

# Set SKIPLIST to skip the instrumentaion of certain files
export SKIPLIST="skip_x.c skip_y.c"

# Set SKIPFUNCS to skip the instrumentation of certain functions
export SKIPFUNCS="main usage"
```
For more information for the types of inserted labels, please refer to [docs/Mutation.md](./docs/Mutation.md).

## Usage: Breaking down...
### Function Side Effect Removal (Normalization)

The tool `src/cannotate/side-effect/side-effect` perform code normalization i.e, removing side-effect statements in condition expressions. For example, function calls and pointer manipulations. 

#### Example 
`input.c`
```c
int foo ( ) ;
....
if (foo()) { }
```
`output.c`
```c
int foo() ;
....
int temp = foo() ;
if (temp) { }
```

##### Usage
```bash
side-effect input.c -o output.c
```
For application, which depends on internal and system libraries to resolve MACROS, we need to specify the PATH to these libraries as follows:

```bash
side-effect input.c -o output.c  -- -I /usr/local/lib/clang/13.0.0/include/ -I /path/to/internal/lib -I /path/to/source/code
```
##### Transforming directly on the source file 
To transform directly on the input file, omitting the use of `-o` will change the input directly. 
```bash
side-effect input.c  -- -I /usr/local/lib/clang/13.0.0/include/ -I /path/to/internal/lib -I /path/to/source/code
```

### CAnnotate Tool 
This tool inputs a `normalized C program` and inserts coverage annotation to the program. 

#### Example
`input.c`
```c
statement_1 ;
if(x==y && a<b)
{...};
statement_3 ;
```
`output.c`
```c
if(x==y){ }
if(x!= y) { }
if(a<b) { }
if(a>=b) { }
if(x==y && a<b)
{...};
statement_3 ;
```
#### Usage 

```bash
cannotate  input.c  --crit="ABS,AOR,COR,ROR,MCC" -o output.c
```

Consider the following normalized C program 
`normalized_input.c`
```c
int foo()
{
    return 0;
}
int main(){
int temp = foo() ;
if (temp>1 && temp<10) { }
return 0;
}

```
```bash
cannotate  normalized_input.c  --crit="ABS,AOR,COR,ROR,MCC" -o normalized_output.c
```
where `--crit` is the coverage criteria.

```
The output of the tool is as follows
`normalized_output.c`

```c
int foo()
{
    return 0;
}
int main(){
int temp = foo() ;
// ABS Label 1
if(temp < 0)  { asm volatile (""::: "memory"); }
// COR Label 2
if((temp > 1 || temp < 10) != (temp > 1 && temp < 10))  { asm volatile (""::: "memory"); }
// ROR Label 3
if((temp <= 1) != (temp > 1))  { asm volatile (""::: "memory"); }
// ROR Label 4
if((temp < 1) != (temp > 1))  { asm volatile (""::: "memory"); }
// ROR Label 5
if((temp >= 1) != (temp > 1))  { asm volatile (""::: "memory"); }
// ROR Label 6
if((temp <= 10) != (temp < 10))  { asm volatile (""::: "memory"); }
// ROR Label 7
if((temp > 10) != (temp < 10))  { asm volatile (""::: "memory"); }
// ROR Label 8
if((temp >= 10) != (temp < 10))  { asm volatile (""::: "memory"); }
// MCC Label 9
if(temp > 1 && temp < 10 ) { asm volatile (""::: "memory"); }
// MCC Label 10
if(temp > 1 && !(temp < 10) ) { asm volatile (""::: "memory"); }
// MCC Label 11
if(!(temp > 1) && temp < 10 ) { asm volatile (""::: "memory"); }
// MCC Label 12
if(!(temp > 1) && !(temp < 10) ) { asm volatile (""::: "memory"); }
if (temp>1 && temp<10) { }
return 0;
}
```
We then compile this program for fuzzing. To measure label coverage during the fuzzing process, we need to track when these labels get triggered. We insert a function call `report_label_coverage()` and use Intel Pintool-A Dynamic Binary Instrumentation to track this coverage. To generate these function calls, we do the following:
```bash
cannotate  normalized_input.c  --crit="ABS,AOR,COR,ROR,MCC" -o normalized_output.c -f cov
```  
the output will have `report_label_coverage()` at every labels as follow:
```c
// ABS Label 1
if(temp < 0)  {
asm volatile (""::: "memory");
report_label_coverage("ABS", 1);
}
```
We can make the function definition of `report_label_coverage()` available during compilation as a separate header file as follows:
`coverage.h`
```c
void __attribute__ ((optimize("O0"))) report_label_coverage(char *crit,int id) asm("report_label_coverage"); 

void __attribute__ ((optimize("O0"))) report_label_coverage(char * crit, int id) {
	asm volatile (""::: "memory");
}
```
Make sure to make `coverage.h` available during compilation. 



